{{#each data.questions}}

<div data-id="{{id}}" class="panel panel-default" style="display:none;">
  <div class="panel-heading">
  </div>
  <div class="panel-body">
    <h3 class="user-question">
      {{name}} {{#if required}}*{{/if}}
    </h3>
    {{#if options.length}}
      {{#ifCond select_type '==' 'single'}}
        {{#each options}}
          <div class="radio">
            <label>
              <input type="radio" name="question{{../id}}" value="{{this.value}}">
              {{this.value}}
            </label>
          </div>
        {{/each}}
      {{/ifCond}}
      {{#ifCond select_type '==' 'multiple'}}
        {{#each options}}
          <div class="checkbox">
            <label>
              <input type="checkbox" name="question{{../id}}" value="{{this.value}}">
              {{this.value}}
            </label>
          </div>
        {{/each}}
      {{/ifCond}}
    {{else}}
      <div class="form-group">
        <input type="text" name="question{{id}}" class="form-control" placeholder="Your answer">
      </div>
    {{/if}}
  </div>
  <div class="panel-footer">
    <button type="button" class="btn answer btn-success">Next</button>
  </div>
</div>
 
{{/each}}
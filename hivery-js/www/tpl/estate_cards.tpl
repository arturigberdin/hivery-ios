{{#each data.estates}}

<div data-id="{{id}}" class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
      {{name}}
    </h3>
  </div>
  <div class="panel-body">
    {{#if preview}}
      <img src="{{preview.url}}"/>
    {{/if}}
  </div>
  <div class="panel-footer">
    <button class="btn btn-danger dislike">Dislike</button>
    <button class="btn btn-warning view">Detail</button>
  </div>
</div>
 
{{/each}}
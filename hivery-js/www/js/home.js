if (localStorage.getItem("token") == null) {
  window.location.href = "index.html";
}

var estates = [];

$(document).ready(function() {

  //this is for jQuery mobile
  $(".ui-loader").hide();

  sendRequest({
    method: "users/mappings?scope=images",
    type: "GET",
    success: function(data) {
      for (var i = data.length - 1; i >= 0; i--) {
        var estate = data[i];
        if (estate.images && estate.images.length > 0) {
          estate.preview = estate.images[0];
        }
        estates.push(estate);
      }
      loadTemplate($(".content"), "estate_cards", {estates: estates}, function(){
        $(".dislike").click(handleDislike);
        $(".view").click(handleView);
        $("body").on("swiperight", handleSwipeRight);
        $("body").on("swipeleft", handleSwipeLeft);
      });
    },
    error: function(err) {}
  });
  $(".like").click(handleLike);

});

function handleDislike(e) {
  var estate = estates[estates.length - 1];
  sendRequest({
    method: "estates/" + estate.id + "/claim",
    type: "POST",
    data: {reaction: "dislike"},
    success: function(data) {
      var estate = estates.pop();
      $("div[data-id='" + estate.id + "']").fadeOut();
    },
    error: function(err) {}
  });
}

function handleView(e) {
  var estate = estates[estates.length - 1];
  var images = "";
  if (estate.images && typeof estate.images != "undefined") {
    for (var i = 0; i < estate.images.length; i++) {
      var image = estate.images[i];
      images += "<img src='" + image.url + "'/>";
    }
  }
  $(".view-estate .modal-title").text(estate.name);
  $(".view-estate .modal-body").html(images);
  $(".view-estate").modal("show");
}

function handleLike(e) {
  var estate = estates[estates.length - 1];
  sendRequest({
    method: "estates/" + estate.id + "/claim",
    type: "POST",
    data: {reaction: "like"},
    success: function(data) {
      var estate = estates.pop();
      $("div[data-id='" + estate.id + "']").fadeOut();
      $(".view-estate").modal("hide");
    },
    error: function(err) {}
  });
}

function handleSwipeRight(e) {
  var modalOpen = $('.view-estate').hasClass('in');
  if (modalOpen) {
    $(".view-estate").modal("hide");
  } else {
    handleDislike();
  }
}

function handleSwipeLeft(e) {
  var modalOpen = $('.view-estate').hasClass('in');
  if (modalOpen) {
    handleLike();
  } else {
    handleView();
  }
}
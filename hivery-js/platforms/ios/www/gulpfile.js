// Initialize gulp.....
var gulp = require('gulp');

// Plugins....
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var watch = require('gulp-watch');

gulp.task('sass', function () {
	gulp.src('./sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch', function() {
    gulp.watch('./sass/main.scss', ['sass']);
});

gulp.task('default', ['sass', 'watch']);


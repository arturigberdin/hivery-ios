if (localStorage.getItem("token") != null) {
  window.location.href = "home.html";
}

var questions = [];
var answers = [];
var user = false;

$(document).ready(function() {

  sendRequest({
    method: "forms/user_questions?scope=headers",
    type: "GET",
    success: function(data) {
      for (var i = data.length - 1; i >= 0; i--) {
        var question = data[i];
        question.nextButton = "Next";
        questions.push(question);
      }
      loadTemplate($(".content"), "user_questions", {questions: questions}, function() {
        showNextQuestion();
        $(".answer").click(handleAnswer);
      });
    },
    error: function(err) {}
  });

  var maskOptions =  {
    onComplete: function(cep) {
      $("#phone").parent().addClass("has-success");
    },
    onChange: function(cep){
      $("#phone").parent().removeClass("has-success");
    },
  };
  $('#phone').mask('+0(000)000-0000', maskOptions);
  $("#phone").focus(function(){
    $(this).parent().removeClass("has-error");
  });
  $("#password").focus(function(){
    $(this).parent().removeClass("has-error");
  });
  

});

function showNextQuestion() {
  if (questions.length > 0) {
    var question = questions[questions.length - 1];
    $("div[data-id='" + question.id + "']").fadeIn();
  } else {
    $("div[data-id='final']").fadeIn();
  }
}

function handleAnswer() {
  if (questions.length == 0) {
    if (user) {
      handleLogin();
    } else {
      handleSubmit();
    }
    return;
  }
  var question = questions[questions.length - 1];
  var answer = {id: question.id, field_name: question.field_name, answer: []};
  if (question.options.length > 0) {
    $("[name='question" + question.id + "']:checked").each(function(){
      answer.answer.push($(this).val());
    })
  } else {
    var value = $("[name='question" + question.id + "']").val().trim();
    if (value.length > 0) {
      answer.answer.push(value);
    }
  }
  if (question.required && answer.answer.length == 0) {
    $("div[data-id='" + question.id + "']").addClass("has-error");
    setTimeout(function() {
      $("div.has-error").bind("click", function(){
        $(this).removeClass("has-error");
        $(this).unbind("click");
      });  
    }, 100);
    return;
  }
  answers.push(answer);
  var question = questions.pop();
  $("div[data-id='" + question.id + "']").fadeOut();
  showNextQuestion();
}

function handleSubmit() {
  var phone = $("#phone").cleanVal();
  if (phone.length < 11) {
    $("#phone").parent().addClass("has-error");
    return;
  }
  phone = '+' + phone;
  var registerData = {
    phone: phone,
    questions: answers
  };
  sendRequest({
    method: "users",
    type: "POST",
    data: registerData,
    success: function(data) {
      user = data;
      $("#phone").attr("disabled", true);
      $("#phone").parent().removeClass("has-success");
      $("[data-id='final'] .answer").text("Sign in");
      $("#password").fadeIn();
      $("#send-alert").fadeIn(200).delay(2000).fadeOut(200);
    },
    error: function(err) {}
  });
}

function handleLogin() {
  var phone = '+' + $("#phone").cleanVal();
  var password = $("#password").val();
  if (password.length < 4) {
    $("#password").parent().addClass("has-error");
    return;
  }
  sendRequest({
    method: "oauth/token",
    type: "POST",
    auth: true,
    data: {
      grant_type: "assertion",
      phone: phone,
      password: password
    },
    success: function(data) {
      localStorage.clear();
      localStorage.setItem("token", data.access_token);
      window.location.href = "home.html";
    },
    error: function(err) {}
  });
}
/*====  Set global variable here ====*/
var config = {
    staging: {
        json_api_url: 'http://rentmama-backend.herokuapp.com'
    },
    local: {
        json_api_url: 'http://localhost:3000'
    }
}

var globalVar = {
    json_api_url : config.staging.json_api_url,
    environment: 'staging'
}
// Local environment
if (window.location.hostname == 'localhost') {
    globalVar.json_api_url = config.local.json_api_url;
    globalVar.environment = 'local';
}

function authUrl(apiMethod) {
    return globalVar.json_api_url + "/" + apiMethod;
}

function apiUrl(apiMethod) {
    return globalVar.json_api_url + "/api/v1/" + apiMethod;
}

function apiAdminUrl(apiMethod) {
    return globalVar.json_api_url + "/api/admin/v1/" + apiMethod;
}

 var requestCounter = 0;
 function addRequest() {
     if (requestCounter++ == 0) {
         $(".loaderContainer").fadeIn();
     }
 }
 function removeRequest() {
     if (--requestCounter < 0) {
         requestCounter = 0;
     }
     if (requestCounter == 0) {
         $(".loaderContainer").fadeOut();
     }
 }
 function resetRequests() {
     requestCounter == 0;
     $(".loaderContainer").fadeOut();
 }
 
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1));
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
    return false;
};

function sendRequest(properties) {
    var token = localStorage.getItem("token");
    var url = apiUrl(properties.method);
    if (properties.admin) {
        url = apiAdminUrl(properties.method);
    }
    if (properties.auth) {
        url = authUrl(properties.method);
    }
    addRequest();
    $.ajax({
        url: url,
        type: properties.type,
        data: properties.data,
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', "Bearer " + token);
        },
        success: function(data) {
            removeRequest();
            if (typeof properties.success != "undefined" && properties.success) {
                properties.success(data);
            }
        },
        error: function(xhr, status, err) {
            resetRequests();
            console.error(properties.method, status, err.toString());
            if (typeof properties.error != "undefined" && properties.error) {
                properties.error(err);
            }
        }
    });
}
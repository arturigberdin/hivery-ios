if (localStorage.getItem("token") == null) {
  window.location.href = "index.html";
} else if (localStorage.getItem("answer_questions")) {
  window.location.href = "questions.html";
}

$(document).ready(function() {

  sendRequest({
    method: "users/mappings",
    type: "GET",
    success: function(data) {
      for (var i = 0; i < data.length; i++) {
        var estate = data[i];
        var images = "";
        for (var j = 0; j < estate.images.length; j++) {
          var image = estate.images[i];
          images += "<img src='" + image.url + "'/>";
        }
        htmlToAppend 
        = "<tr>"
				+ "<td>" + estate.name + "</td>"
				+ "<td>" + images + "</td>"
				+ "</tr>"
        $("#estates_body").append(htmlToAppend);
      }
    },
    error: function(err) {}
  });

});
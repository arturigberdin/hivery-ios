if (localStorage.getItem("token") != null) {
  if (localStorage.getItem("answer_questions")) {
    window.location.href = "questions.html";
  } else {
    window.location.href = "home.html";
  }
}

$(document).ready(function() {

  $("input").focus(function(){
    $(this).parent().removeClass("has-error");
  });
  
  function checkPassword() {
    var password = $("#password").val();
    if (password.length < 4) {
      $("#password").parent().addClass("has-error");
      return false;
    }
    return password;
  }

  function checkPhone() {
    var phone = $("#phone").cleanVal();
    if (phone.length < 11) {
      $("#phone").parent().addClass("has-error");
      return false;
    }
    return '+' + phone;
  }

  var maskOptions =  {
    onComplete: function(cep) {
      $("#phone").parent().addClass("has-success");
    },
    onChange: function(cep){
      $("#phone").parent().removeClass("has-success");
    },
  };
  $('#phone').mask('+0(000)000-0000', maskOptions);

  $("#send-pass-btn").click(function(e) {
    e.preventDefault();
    var phone = checkPhone();
    if (!phone) {
      return;
    }
    sendRequest({
      method: "users",
      type: "POST",
      data: {phone: phone},
      success: function(data) {},
      error: function(err) {}
    });
  });

  $("#login-btn").click(function(e) {
    e.preventDefault();
    var phone = checkPhone();
    var password = checkPassword();
    if (!password || !password) {
      return;
    }
    sendRequest({
      method: "oauth/token",
      type: "POST",
      auth: true,
      data: {
        grant_type: "assertion",
        phone: phone,
        password: password
      },
      success: function(data) {
        localStorage.clear();
        localStorage.setItem("phone", phone);
        localStorage.setItem("token", data.access_token);
        if (!data.poll_completed) {
          localStorage.setItem("answer_questions", true);
          window.location.href = "questions.html";
        } else {
          window.location.href = "home.html";
        }
      },
      error: function(err) {}
    });
  });

});
if (localStorage.getItem("token") == null) {
  window.location.href = "index.html";
} else if (!localStorage.getItem("answer_questions")) {
  window.location.href = "home.html";
}

$(document).ready(function() {

  var url = $(".typeform-widget").data("url");
  url += $.param({phone: localStorage.getItem("phone")});
  $(".typeform-widget").data("url", url); 
  
  //Typeform code
  (function(){var qs,js,q,s,d=document,gi=d.getElementById,
  ce=d.createElement,gt=d.getElementsByTagName,id='typef_orm',b=
  'https://s3-eu-west-1.amazonaws.com/share.typeform.com/widget.js';
  if(!gi.call(d,id)){js=ce.call(d,'script');js.id=id;js.src=b;
  q=gt.call(d,'script')[0];q.parentNode.insertBefore(js,q)}})()

  $(window).on('message', function(ev) {
    if(ev.originalEvent.data === "form-submit") {
      
      sendRequest({
        method: "users/update",
        type: "PUT",
        data: {poll_completed: true},
        success: function(data) {
          localStorage.removeItem("answer_questions");
          window.location.href = "home.html";
        },
        error: function(err) {}
      });
      
    }
  });
});
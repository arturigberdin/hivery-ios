//
//  AIRQuestionsViewController+Navigation.h
//  hivery
//
//  Created by Artur on 14/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRSetUpProfileVC.h"

@interface AIRSetUpProfileVC (Navigation)

- (void)showEstatesControllerInTabBar;

@end

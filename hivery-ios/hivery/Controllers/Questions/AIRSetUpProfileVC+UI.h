//
//  AIRQuestionsViewController+UI.h
//  hivery
//
//  Created by Artur on 14/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRSetUpProfileVC.h"

@interface AIRSetUpProfileVC (UI)

@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UILabel *setupYourProfileLabel;

- (void)showNavigationControls;
- (void)hideNavigationControls;

@end

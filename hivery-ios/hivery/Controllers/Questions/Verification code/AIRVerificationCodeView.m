//
//  AIRPinCodeView.m
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRVerificationCodeView.h"

//Custom Controls
#import "TTTAttributedLabel.h"

//Services
#import "AIRAPIManager.h"

@interface AIRVerificationCodeView () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *attributedLabel;

@property (weak, nonatomic) IBOutlet UIView *pinCodeContainer;
@property (weak, nonatomic) IBOutlet UIView *cardContainer;

@property (copy, nonatomic) AIRPincodeBlock pincodeBlock;

//Pin Views
@property (weak, nonatomic) IBOutlet UIImageView *pinView1;
@property (weak, nonatomic) IBOutlet UIImageView *pinView2;
@property (weak, nonatomic) IBOutlet UIImageView *pinView3;
@property (weak, nonatomic) IBOutlet UIImageView *pinView4;

@property (weak, nonatomic) IBOutlet UIButton *resendVerificationCodeButton;

@property (weak, nonatomic) IBOutlet UITextField *pincodeTextField;

@end

@implementation AIRVerificationCodeView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupNib];
        
        self.pincodeTextField.delegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(setupQuestion:)
                                                     name:kVerificationCode
                                                   object:nil];
    }
    return self;
}

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRVerificationCodeView.class) owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = ScreenBounds;
}

- (void)setupQuestion:(NSNotification *)notification {
    
    NSString *phone;
    if ([notification.name isEqualToString:kVerificationCode]) {
        NSDictionary *userInfo = notification.userInfo;
        phone = (NSString *)userInfo[@"phone"];
    }
   
    NSString *text = [NSString stringWithFormat:@"Please enter your verification code that was sent to %@", phone];
    NSArray *stringsArray = [text componentsSeparatedByString:@"to"];
    
    UIFont *font = [UIFont fontWithName:@"Bitter-Regular" size:21];
    UIColor *color = UIColorFromRGB(0x34495E);
    
    NSMutableAttributedString *mutableString =
    [[NSMutableAttributedString alloc]
     initWithString:text
     attributes: @{
                   NSFontAttributeName : font,
                   NSForegroundColorAttributeName : color
                   }];
    
    NSRange numberRange = [[mutableString string] rangeOfString:stringsArray[1] options:NSCaseInsensitiveSearch];
    [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:numberRange];
    
    self.attributedLabel.text = mutableString;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self makeFirstResponder];
    });
}

- (void)layoutSubviews {
    
    if (IS_IPHONE_5_OR_LESS) {
        [self.pinCodeContainer updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.75);
        }];
        [self.cardContainer updateConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
        }];
    } else {
        [self.pinCodeContainer updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.65);
        }];
    }
    
    [super layoutSubviews];
}

#pragma mark - Entry point

- (void)answerWithCompletion:(AIRPincodeBlock)completion {
    self.pincodeBlock = completion;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string length] == 0 && range.length > 0) {
        DLog(@"Some characters deleted");
        [self displayPinNumber:(textField.text.length - 1)];
        return YES;
    }
    
    BOOL validNumber = [AIRValidator validateNumber:string];
    if (!validNumber) return NO;

    [self displayPinNumber:(textField.text.length + 1)];
    
    if (textField.text.length + 1 == 5) {
        return NO;
    }
    
    return YES;
}

- (void)displayPinNumber:(NSInteger)number {
    
    if (number == 0) {
        self.pinView1.image = [UIImage imageNamed:@"Pin"];
        self.pinView2.image = [UIImage imageNamed:@"Pin"];
        self.pinView3.image = [UIImage imageNamed:@"Pin"];
        self.pinView4.image = [UIImage imageNamed:@"Pin"];
    }
    if (number == 1) {
        self.pinView1.image = [UIImage imageNamed:@"PinFull"];
        self.pinView2.image = [UIImage imageNamed:@"Pin"];
        self.pinView3.image = [UIImage imageNamed:@"Pin"];
        self.pinView4.image = [UIImage imageNamed:@"Pin"];
    }
    if (number == 2) {
        self.pinView1.image = [UIImage imageNamed:@"PinFull"];
        self.pinView2.image = [UIImage imageNamed:@"PinFull"];
        self.pinView3.image = [UIImage imageNamed:@"Pin"];
        self.pinView4.image = [UIImage imageNamed:@"Pin"];
    }
    if (number == 3) {
        self.pinView1.image = [UIImage imageNamed:@"PinFull"];
        self.pinView2.image = [UIImage imageNamed:@"PinFull"];
        self.pinView3.image = [UIImage imageNamed:@"PinFull"];
        self.pinView4.image = [UIImage imageNamed:@"Pin"];
    }
    if (number == 4) {
        self.pinView1.image = [UIImage imageNamed:@"PinFull"];
        self.pinView2.image = [UIImage imageNamed:@"PinFull"];
        self.pinView3.image = [UIImage imageNamed:@"PinFull"];
        self.pinView4.image = [UIImage imageNamed:@"PinFull"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [[AIRAPIManager sharedAPI]
             confirmCode:[self.pincodeTextField.text integerValue]
             phone:[[NSUserDefaults standardUserDefaults] objectForKey:kPhoneNumber]
             callback:^(BOOL isConfirmed, NSError *error) {
                 
                 if (isConfirmed) {
                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserAuthorized];
                     [self.view endEditing:YES];
                     DLog(@"register new user request");
                     if (self.pincodeBlock) {
                         self.pincodeBlock(self.pincodeTextField.text);
                     }
                 } else {
                     self.pincodeTextField.text = @"";
                     [self displayPinNumber:0];
                     
                     [self displayConfirmationAlert];
                 }
             }];
        });
    }
}

- (void)makeFirstResponder {
    [self.pincodeTextField becomeFirstResponder];
}

#pragma mark - Events

- (IBAction)resendVerificationCodePressed:(id)sender {
    
    NSString *phone = [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneNumber];
    
    [[AIRAPIManager sharedAPI]
     restoreVerificationCodeWithPhone:phone
     callback:^(NSString *verificationCode, NSError *error) {
         
         DLog(@"verification_code = %@", verificationCode);
     }];
}

#pragma mark - Private

- (void)displayConfirmationAlert {
    [AIRAlert alertCodeConfirmation];
}

@end

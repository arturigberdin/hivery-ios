//
//  AIRPinCodeView.h
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIRVerificationCodeView : UIView

typedef void (^AIRPincodeBlock)(NSString *pincode);

- (void)answerWithCompletion:(AIRPincodeBlock)completion;

- (void)makeFirstResponder;

@end

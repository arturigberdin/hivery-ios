//
//  AIRSingleQuestionView.h
//  hivery
//
//  Created by Artur on 23/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

//Models
#import "AIRQuestion.h"
#import "AIRAnswer.h"

typedef void (^AIRAnswerHandler)(AIRAnswer *answer);

@interface AIRSingleQuestionView : UIView

/*
 View with question and answers list with single choice
 */

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet SWFrameButton *nextQuestionButton;

- (void)setupWithQuestion:(AIRQuestion *)question
               completion:(void(^)(AIRAnswer *answer))completion;

@end

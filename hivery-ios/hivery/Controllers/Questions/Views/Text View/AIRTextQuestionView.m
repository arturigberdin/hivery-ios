//
//  AIRTextQuestionView.m
//  hivery
//
//  Created by Artur on 23/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRTextQuestionView.h"

//Models
#import "AIRQuestion.h"
#import "AIRAnswer.h"

@interface AIRTextQuestionView ()<UITextFieldDelegate>

//Outlets
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;
@property (weak, nonatomic) IBOutlet SWFrameButton *nextButton;

//Blocks
@property (copy, nonatomic) AIRAnswerHandler answerHandler;
@property (copy, nonnull) AIRTextHandler textHandler;

//Containers
@property (strong, nonatomic) AIRQuestion *question;

@end

@implementation AIRTextQuestionView

//Override if View inside IB
- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setupNib];
    }
    return self;
}

//Override if View call init
- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupNib];
    }
    return self;
}

#pragma mark - Setups

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRTextQuestionView.class) owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = ScreenBounds;
}

#pragma mark - Entry point

- (void)setupWithQuestion:(AIRQuestion *)question
               completion:(void(^)(AIRAnswer *answer))completion {
    
    self.answerHandler = completion;
    
    self.question  = question;
    self.questionLabel.text = question.name;
    self.answerTextField.placeholder = question.fieldName;
    
    self.answerTextField.delegate = self;
    
    [self.nextButton addTarget:self action:@selector(nextPressed:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)phoneWithCompletion:(void(^)(NSString *text))completion {
    self.textHandler = completion;
    
    self.questionLabel.text = @"Phone number *";
    
    self.answerTextField.placeholder = @"Phone number";
    self.answerTextField.delegate = self;
    
    self.nextButton.tintColor = [UIColor greenColor];
    [self.nextButton setTitle:@"Register" forState:UIControlStateNormal];
    [self.nextButton addTarget:self action:@selector(registerPressed:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)passcodeWithPhone:(NSString *)phone
               completion:(void(^)(NSString *text))completion {
    
    self.textHandler = completion;
    
    self.questionLabel.text = @"Enter passcode for your phone";
    
    self.answerTextField.placeholder = @"Passcode";
    self.answerTextField.delegate = self;
    
    self.nextButton.tintColor = [UIColor redColor];
    [self.nextButton setTitle:@"Sign In" forState:UIControlStateNormal];
    
    [self.nextButton addTarget:self action:@selector(signInPressed:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Events

- (IBAction)nextPressed:(id)sender {
    
    BOOL validAnswer = [self validateAnswer];
    
    if (validAnswer) {
        AIRAnswer *answer = [[AIRAnswer alloc] init];
        
        answer.questionID = self.question.questionID;
        answer.fieldName = self.question.fieldName;
        answer.answers = @[self.answerTextField.text];
        answer.answerIndex = (self.question.priority - 1);
        
        if (self.answerHandler) {
            self.answerHandler(answer);
        }
    }
}

- (void)registerPressed:(id)sender {

    BOOL validPhone = [AIRValidator validatePhone:self.answerTextField.text];
    if (validPhone) {
        if(self.textHandler) {
            self.textHandler(self.answerTextField.text);
        }
    }
}

- (void)signInPressed:(id)sender {
    
    BOOL validPasscode = YES; //[AIRValidator validatePhone:self.answerTextField.text];
    if (validPasscode) {
        if(self.textHandler) {
            self.textHandler(self.answerTextField.text);
        }
    }
}

#pragma mark - Actions

- (BOOL)validateAnswer {
    
    if (self.answerTextField.text.length > 0) {
        return YES;
    }
    return NO;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.answerTextField resignFirstResponder];
    return YES;
}

@end

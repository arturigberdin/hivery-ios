//
//  AIRTextQuestionView.h
//  hivery
//
//  Created by Artur on 23/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AIRQuestion;
@class AIRAnswer;

/*
 View with question and answer in text field
*/

typedef void (^AIRAnswerHandler)(AIRAnswer *answer);
typedef void (^AIRTextHandler)(NSString *text);

@interface AIRTextQuestionView : UIView

- (void)setupWithQuestion:(AIRQuestion *)question
           completion:(void(^)(AIRAnswer *answer))completion;

- (void)phoneWithCompletion:(void(^)(NSString *text))completion;

- (void)passcodeWithPhone:(NSString *)phone
               completion:(void(^)(NSString *text))completion;

@end

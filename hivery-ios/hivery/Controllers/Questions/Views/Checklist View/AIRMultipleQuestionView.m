//
//  AIRMultipleQuestionView.m
//  hivery
//
//  Created by Artur on 23/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRMultipleQuestionView.h"

//Models
#import "AIROption.h"

//Controls
#import "SWFrameButton.h"

@interface AIRMultipleQuestionView ()

@property (copy, nonatomic) AIRAnswerHandler answerHandler;

@property (strong, nonatomic) AIRQuestion *question;

@property (strong, nonatomic) NSMutableArray *buttonsContainer;

@end

@implementation AIRMultipleQuestionView

//Override if View call init
- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupNib];
    }
    return self;
}

#pragma mark - Private

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRMultipleQuestionView.class) owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = ScreenBounds;
}

#pragma mark - Public

- (void)setupWithQuestion:(AIRQuestion *)question
              completion:(void (^)(AIRAnswer *))completion {
    
    self.answerHandler = completion;
    
    self.question  = question;
    self.questionLabel.text = question.name;
    
    self.buttonsContainer = [NSMutableArray new];
    
    int i = 0;
    for (AIROption *option in question.options) {
        
        i++;
        DLog(@"option = %@, index = %d", option.value, i);
        
        SWFrameButton *button = [[SWFrameButton alloc] init];
        
        [button setTitle:option.value forState:UIControlStateNormal];
        button.tintColor = [UIColor orangeColor];
        button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        button.frame = CGRectMake(ScreenWidth/2 - 150, ScreenHeight*i/13 + 10, 300, 38);
        
        [button addTarget:self action:@selector(toggleSelection:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:button];
        
        [self.buttonsContainer addObject:button];
    }
}

#pragma mark - Events

- (void)toggleSelection:(id)sender {
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
}

- (IBAction)nextPressed:(id)sender {
    
    BOOL validAnswer = [self validateAnswer];
    
    if (validAnswer) {
        AIRAnswer *answer = [[AIRAnswer alloc] init];
        
        answer.questionID = self.question.questionID;
        answer.fieldName = self.question.fieldName;
        answer.answerIndex = (self.question.priority - 1);
        
        NSMutableArray *answersArray = [NSMutableArray new];
        for (UIButton *button in self.buttonsContainer) {
            if (button.selected) {
                NSString *object = button.titleLabel.text;
                [answersArray addObject:object];
            }
        }
        answer.answers = answersArray;
        
        if (self.answerHandler) {
            self.answerHandler(answer);
        }
    }
}

#pragma mark - Actions

- (BOOL)validateAnswer {
    
    for (UIButton *button in self.buttonsContainer) {
        if (button.selected) {
            return YES;
        }
    }
    return NO;
}




@end

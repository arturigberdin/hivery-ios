//
//  AIRNameEmailView+Layout.m
//  hivery
//
//  Created by Artur on 14/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRNameEmailView+Layout.h"

@implementation AIRNameEmailView (Layout)

@dynamic nameContainer, emailContainer;

- (void)updateConstraints {
    
    if (IS_IPHONE_5_OR_LESS) {
        [self.nameContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@5);
            make.right.equalTo(@5);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.45);
        }];
        
        [self.emailContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@5);
            make.right.equalTo(@5);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.9);
        }];
    } else {
        [self.nameContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@20);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.45);
        }];
        
        [self.emailContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@20);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.9);
        }];
    }
    [super updateConstraints];
}

@end

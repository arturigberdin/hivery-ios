//
//  AIRNameEmailView.m
//  hivery
//
//  Created by Artur on 31/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRNameEmailView.h"

//Categories
#import "AIRNameEmailView+Layout.h"

@interface AIRNameEmailView () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UIView *nameContainer;
@property (weak, nonatomic) IBOutlet UIView *emailContainer;

@property (copy, nonatomic) AIRAnswerBlock answerBlock;

@end

@implementation AIRNameEmailView

#pragma mark - Initialize

//Override if View call init
- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupNib];
        
        self.nameTextField.delegate = self;
        self.emailTextField.delegate = self;
    }
    return self;
}

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRNameEmailView.class) owner:self options:nil];
    [self addSubview:self.view];
    
    self.view.frame = ScreenBounds;
}

#pragma mark - Entry point

- (void)answerWithCompletion:(AIRAnswerBlock)completion {
    self.answerBlock = completion;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    BOOL validName = [AIRValidator validateName:self.nameTextField.text];
    BOOL validEmail = [AIRValidator validateEmail:self.emailTextField.text];
    
    if ([self.nameTextField isFirstResponder]) {
        if (validName) {
            [self.emailTextField becomeFirstResponder];
        }
    }
    
    if ([self.emailTextField isFirstResponder]) {
        if (validEmail) {
        }
    }
    
    if (validName && validEmail) {
        DLog(@"Show Next Question");
        
        [self.view endEditing:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.answerBlock) {
                self.answerBlock(self.nameTextField.text, self.emailTextField.text);
            }
        });
        
    } else {

        NSMutableString *alertString = [NSMutableString new];
        
        NSString *name = self.nameTextField.text;
        NSString *email = self.emailTextField.text;
        name = [name trimmingWhitespaces];
        email = [email trimmingWhitespaces];
        
        //text was empty or only whitespace
        if (name.length == 0) {
            [alertString appendString:@"Name is required"];
        } else if (!validName) {
            [alertString appendString:@"Invalid name"];
        }
        
        if (email.length == 0) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Email is required"];
        } else if (!validEmail) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Invalid email"];
        }

        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:alertString message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            NSLog(@"ALERT!");
        }]];
        
        [alert show];
    }
    return YES;
}

@end

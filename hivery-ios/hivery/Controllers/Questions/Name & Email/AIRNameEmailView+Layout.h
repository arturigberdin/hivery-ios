//
//  AIRNameEmailView+Layout.h
//  hivery
//
//  Created by Artur on 14/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRNameEmailView.h"

@interface AIRNameEmailView (Layout)

@property (nonatomic) UIView *nameContainer;
@property (nonatomic) UIView *emailContainer;

@end

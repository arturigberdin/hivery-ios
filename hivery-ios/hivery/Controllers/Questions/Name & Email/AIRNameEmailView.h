//
//  AIRNameEmailView.h
//  hivery
//
//  Created by Artur on 31/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^AIRAnswerBlock)(NSString *name, NSString *email);

@interface AIRNameEmailView : UIView

- (void)answerWithCompletion:(AIRAnswerBlock)completion;

@end

//
//  AIRQuestionsViewController+Navigation.m
//  hivery
//
//  Created by Artur on 14/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRSetUpProfileVC+Navigation.h"

//Controllers
#import "AIRProfileViewController.h"
#import "AIREstatesViewController.h"
#import "AIRScheduleViewController.h"

//Base Controllers
#import "AIRBaseTabBarController.h"
#import "AIRProfileNavigationController.h"

@implementation AIRSetUpProfileVC (Navigation)

#pragma mark - Navigation

- (void)showEstatesControllerInTabBar {
    
    AIRProfileViewController *vc1 = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRProfileViewController.class)];
    AIREstatesViewController *vc2 = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIREstatesViewController.class)];
    AIRScheduleViewController *vc3 = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRScheduleViewController.class)];
    
    AIRProfileNavigationController *navVC1 = [[AIRProfileNavigationController alloc] initWithRootViewController:vc1];
    AIRProfileNavigationController *navVC3 = [[AIRProfileNavigationController alloc] initWithRootViewController:vc3];
    
    AIRBaseTabBarController *tabVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRBaseTabBarController.class)];
    tabVC.viewControllers = [NSArray arrayWithObjects:navVC1, vc2, navVC3, nil];
    
    [self presentViewController:tabVC animated:YES completion:nil];
    
    //Show second tab in tabbar
    [tabVC setSelectedIndex:1];
}


@end

//
//  AIRAreasQuestionView.m
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAreasQuestionView.h"

#import "AIRAccount.h"

#ifdef IS_IPHONE_5_OR_LESS
#define BUTTON_WIDTH 310
#else
#define BUTTON_WIDTH 320
#endif

#define BUTTON_HEIGHT 50

#define CELL_HEIGHT   70


@interface AIRAreasQuestionView () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *areasContainer;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *districts;

@property (strong, nonatomic) NSMutableArray *districtsSelected;

@property (strong, nonatomic) NSMutableArray *buttonsContainer;

@property (copy, nonatomic) AIRDistrictBlock answerBlock;

@property (weak, nonatomic) IBOutlet AXWireButton *nextButton;

@property (strong, nonatomic) AIRAccount *account; //работаем с districts

@end

@implementation AIRAreasQuestionView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupNib];
        
        DLog(@"BUTTON_WIDTH = %d", BUTTON_WIDTH);
        
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
        _tableView.separatorColor = [UIColor clearColor];
        
        _districts = [NSMutableArray new];
        
        _districtsSelected = [NSMutableArray new];
        
        _districts = [@[@"District 1 - Raffles Place & Marina Bay",
                        @"District 2 - Tanjong Pagar & Chinatown",
                        @"District 3 - Tiong Bahru & Queenstown",
                        @"District 4 - Telok Blangah & Harbourfront",
                        @"District 5 - Buona Vista, Pasir Panjang & Clementi",
                        @"District 6 - Clarke Quay & City Hall",
                        @"District 7 - Bugis & Beach Road",
                        @"District 8 - Little India & Farrer Park",
                        @"District 9 - Orchard Road & River Valley",
                        @"District 10 - Bukit Timah & Holland",
                        @"District 11 - Novena, Newton, & Thomson"
                        ] mutableCopy];
        
        [self setupButtons];
        
    }
    return self;
}

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRAreasQuestionView.class) owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = ScreenBounds;
}

- (void)setupWithAccount:(AIRAccount *)account {
    
    self.account = account;
    
    //Take massive from account
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.districtsSelected = self.account.districtsContainer;
        
        for (NSString *district in self.districtsSelected) {
            
            for (AXWireButton *button in self.buttonsContainer) {
                
                if ([button.titleLabel.text isEqualToString:district]) {
                    button.selected = YES;
                }
            }
        }
        
        [self.tableView reloadData];
    });
}

- (void)setupButtons {
    
    self.buttonsContainer = [NSMutableArray new];
    
    for (NSString *item in self.districts) {
        
        static NSInteger index = 0;
        index ++;
        
        AXWireButton *districtButton = [[AXWireButton alloc] init];
        
        [districtButton setTitle:item forState:UIControlStateNormal];
        
        [districtButton setTitleColor:UIColorFromRGB(0x667190) forState:UIControlStateNormal];
        [districtButton setTitleColor:UIColorFromRGB(0xF8854F) forState:UIControlStateSelected];
        
        [districtButton setBackgroundImage:[UIImage imageWithUIColor:[UIColor clearColor]] forState:UIControlStateNormal];
        [districtButton setBackgroundImage:[UIImage imageWithUIColor:[UIColor whiteColor]] forState:UIControlStateSelected];
        
        districtButton.titleLabel.font = [UIFont fontWithName:@"Gotham Rounded" size:15];
        
        districtButton.titleLabel.numberOfLines = 0;
        districtButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        if ((index) == 5) {
            districtButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        } else {
            districtButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        }
        
        [self.buttonsContainer addObject:districtButton];
    }
    
}

- (void)layoutSubviews {
    
    if (IS_IPHONE_5_OR_LESS) {
        [self.areasContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            //make.top.equalTo(self.mas_);
            //make.bottom.equalTo(self.mas_);
        }];
    } else {
        [self.areasContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@20);
            make.top.equalTo(@20);
            make.bottom.equalTo(@100);
        }];
    }
    
    [super layoutSubviews];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.districts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    AXWireButton *districtButton = self.buttonsContainer[indexPath.row];
    
    [districtButton setTintColor:UIColorFromRGB(0xF8854F)];
    
    CGFloat cellWidth = self.tableView.frame.size.width;
    districtButton.frame = CGRectMake((cellWidth - BUTTON_WIDTH) / 2,
                                      (CELL_HEIGHT - BUTTON_HEIGHT) / 2,
                                      BUTTON_WIDTH, BUTTON_HEIGHT);
    [cell.contentView addSubview:districtButton];
    
    [districtButton addTarget:self action:@selector(toggleSelection:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return CELL_HEIGHT;
}

#pragma mark - Entry point

- (void)answerWithCompletion:(AIRDistrictBlock)completion {
    self.answerBlock = completion;
}

#pragma mark - Events

- (void)toggleSelection:(UIButton *)sender {
    UIButton *button = sender;
    
    button.selected = !button.selected;
    
    if (button.selected) {
        //select - add item
        [self.districtsSelected addObject:button.titleLabel.text];
    } else {
        NSMutableArray *options = [self.districtsSelected mutableCopy];
        //deselect - remove item
        for (NSString *item in self.districtsSelected) {
            if ([item isEqualToString:button.titleLabel.text]) {
                [options removeObject:item];
            }
        }
        self.districtsSelected = options;
    }
    
    DLog(@"districts = %@", self.districtsSelected);
}

- (IBAction)nextButtonPressed:(id)sender {
    [self sendAnswerBlock];
}

- (void)sendAnswerBlock {
    if (self.answerBlock) {
        self.answerBlock(self.districtsSelected);
    }
}

@end

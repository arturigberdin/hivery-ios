//
//  AIRAreasQuestionView.h
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

//Models
@class AIRAccount;

@interface AIRAreasQuestionView : UIView

typedef void (^AIRDistrictBlock)(NSArray *options);

- (void)answerWithCompletion:(AIRDistrictBlock)completion;

//Entry point
- (void)setupWithAccount:(AIRAccount *)account;

- (void)sendAnswerBlock;

@end

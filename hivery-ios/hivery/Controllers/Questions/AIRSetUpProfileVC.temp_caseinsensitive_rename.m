//
//  AIRQuestionsViewController.m
//  hivery
//
//  Created by Artur on 22/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRSetupProfileVC.h"

//API
#import "AIRAPIManager.h"

//Models
#import "AIRAccount.h"

//Setup Views
#import "AIRNameEmailView.h"
#import "AIRPhonePasswordView.h"
#import "AIRPinCodeView.h"
#import "AIRBedroomQuestionView.h"
#import "AIRAreasQuestionView.h"
#import "AIRFinishQuestionView.h"

//Categories
#import "AIRQuestionsViewController+Navigation.h"
#import "AIRQuestionsViewController+UI.h"


@interface AIRQuestionsViewController () <UIScrollViewDelegate>

//Scrollview
@property (strong, nonatomic) UIScrollView *scrollView;
@property (assign, nonatomic) NSInteger pageCount;

//Containers
@property (strong, nonatomic) NSMutableArray *paramsContainer; //params

//Model
@property (strong, nonatomic) AIRAccount *account;

//Above all subviews UI elements
@property (weak, nonatomic) IBOutlet UILabel *setupYourProfileLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation AIRQuestionsViewController

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Containers
    self.paramsContainer = [NSMutableArray new];
    self.account = [[AIRAccount alloc] init];
    
    //UI
    [self setupUI];
}

#pragma mark - Core

- (void)prepareScrollView {
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:ScreenBounds];
    [self.view addSubview:self.scrollView];
    
    self.scrollView.delegate = self;
    
    //Name & Email [] []
    AIRNameEmailView *nameEmailView = [[AIRNameEmailView alloc] init];
    nameEmailView.frame = CGRectMake(0 * ScreenWidth, 0, ScreenWidth, ScreenHeight);
    
    [nameEmailView answerWithCompletion:^(NSString *name, NSString *email) {
        DLog(@"answer = %@, %@", name, email);
        self.account.name = name;
        self.account.email = email;
        [self scrollToPage:1];
    }];
    
    AIRPhonePasswordView *phonePasswordView = [[AIRPhonePasswordView alloc] init];
    phonePasswordView.frame = CGRectMake(1 * ScreenWidth, 0, ScreenWidth, ScreenHeight);
    
    [phonePasswordView answerWithCompletion:^(NSString *phone, NSString *password) {
        DLog(@"answer = %@, %@", phone, password);
        
        self.account.phone = phone;
        self.account.password = password;
        
        [self authorizeWithPhone];
        
        [[AIRAPIManager sharedAPI]
         registerWithUsername:self.account.name
         email:self.account.email
         phone:self.account.phone
         password:self.account.password
         callback:^(NSString *verification_code, NSError *error) {
             
             [self scrollToPage:2];
             [self passPhoneToPasscodeView:self.account.phone delay:0.25];
          }];
    }];
    
    AIRPinCodeView *pinCodeView = [[AIRPinCodeView alloc] init];
    pinCodeView.frame = CGRectMake(2 * ScreenWidth, 0, ScreenWidth, ScreenHeight);
    
    [pinCodeView answerWithCompletion:^(NSString *pincode) {
        DLog(@"pincode = %@", pincode);
        [self scrollToPage:3];
    }];
    
    AIRBedroomQuestionView *bedroomView = [[AIRBedroomQuestionView alloc] init];
    bedroomView.frame = CGRectMake(3 * ScreenWidth, 0, ScreenWidth, ScreenHeight);
    
    [bedroomView answerWithCompletion:^(NSInteger bedroom) {
        [self scrollToPage:4];
    }];
    
    AIRAreasQuestionView *areasView = [[AIRAreasQuestionView alloc] init];
    areasView.frame = CGRectMake(4 * ScreenWidth, 0, ScreenWidth, ScreenHeight);
    
    [areasView answerWithCompletion:^(NSArray *options) {
        [self scrollToPage:5];
    }];

    AIRFinishQuestionView *finishView = [[AIRFinishQuestionView alloc] init];
    finishView.frame = CGRectMake(5 * ScreenWidth, 0, ScreenWidth, ScreenHeight);
    
    [finishView answerWithCompletion:^(BOOL setupProfileFinished) {
        if (setupProfileFinished) {
            [self showEstatesController];
        }
    }];

    [self.scrollView addSubview:nameEmailView];
    [self.scrollView addSubview:pinCodeView];
    [self.scrollView addSubview:phonePasswordView];
    [self.scrollView addSubview:bedroomView];
    [self.scrollView addSubview:areasView];  //mutliple
    [self.scrollView addSubview:finishView];

    self.scrollView.contentSize = CGSizeMake(ScreenWidth * 6, ScreenHeight);
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.scrollEnabled = YES;
}

- (void)setupUI {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self prepareScrollView];
        [self showNavigationControls];
    });
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    DLog(@"velocity = %@", NSStringFromCGPoint(velocity));
    DLog(@"targetContentOffset was = %@", NSStringFromCGPoint(*targetContentOffset));
    targetContentOffset->y = [self indexForCGPoint:scrollView.contentOffset withVelocity:velocity] * self.view.frame.size.height;
    DLog(@"targetContentOffset became = %@", NSStringFromCGPoint(*targetContentOffset));
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float fractionalPage = scrollView.contentOffset.x / ScreenWidth;
    NSInteger page = lround(fractionalPage);
    DLog(@"%ld",(long)page);
    self.pageCount = page;
}

- (NSUInteger)indexForCGPoint:(CGPoint)targetContentOffset withVelocity:(CGPoint)velocity {
    NSInteger delta = (velocity.y > 0) ? 1 : 0;
    NSUInteger index = (targetContentOffset.y / self.view.frame.size.height) + delta;
    return index;
}

- (void)scrollToPage:(NSInteger)pageNumber {
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * pageNumber;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

#pragma mark - Events

- (IBAction)backButtonPressed:(id)sender {
    if (self.pageCount > 0) {
        [self scrollToPage:(self.pageCount - 1)];
        self.pageCount--;
        DLog(@"page = %ld", self.pageCount);
    }
}

- (void)passPhoneToPasscodeViewNotificaiton{
    
}

- (void)changeAccessTokenNotification {
    
}

#pragma mark - API

- (void)authorizeWithPhone {
    [[AIRAPIManager sharedAPI]
     authorizeWithPhone:self.account.phone
     password:self.account.password
     callback:^(NSString *accessToken, NSError *error) {
         
         BOOL isStored = [self saveAccessTokenToKeychain:accessToken account:self.account.phone];
         if (isStored) {
             NSString *token = [self takeAccessTokenFromKeychainAccount:self.account.phone];
             [self changeAccessToken:token];
         }
     }];
}

- (void)registerAccount:(void(^)(BOOL complete))completion {
    
}

- (void)saveProfileInfo:(void(^)(BOOL complete))completion {
    //    [[AIRAPIManager sharedAPI]
    //     postProfileInfoWithPhone:self.phoneNumber
    //     questions:[self.paramsContainer copy]
    //     callback:^(AIRAccount *account, NSError *error) {
    //
    //         if (account) {
    //             DLog(@"account = %@", account);
    //             completion(YES);
    //         }
    //     }];
}

#pragma mark - Token

- (void)passPhoneToPasscodeView:(NSString *)phone delay:(CGFloat)delay {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSDictionary *userInfo = @{@"phone": phone};
        [[NSNotificationCenter defaultCenter] postNotificationName:kVerificationCode object:self userInfo:userInfo];
    });
}

- (BOOL)saveAccessTokenToKeychain:(NSString *)accessToken account:(NSString *)phone {
    BOOL result = [SAMKeychain setPassword:accessToken forService:@"com.hivery.token" account:phone];
    return result;
}

- (NSString *)takeAccessTokenFromKeychainAccount:(NSString *)phone {
    NSString *token = [SAMKeychain passwordForService:@"com.hivery.token" account:phone];
    DLog(@"takeAccessTokenFromKeychain = %@",token);
    return token;
}

- (void)changeAccessToken:(NSString *)accessToken {
    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeAccessToken object:accessToken];
}

@end

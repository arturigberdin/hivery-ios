//
//  AIRFinishQuestionView.h
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIRFinishQuestionView : UIView

typedef void (^AIRSetupBlock)(BOOL setupProfileFinished);

- (void)answerWithCompletion:(AIRSetupBlock)completion;

@end

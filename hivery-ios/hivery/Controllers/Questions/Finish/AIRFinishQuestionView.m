//
//  AIRFinishQuestionView.m
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRFinishQuestionView.h"

//API
#import "AIRAPIManager.h"

@interface AIRFinishQuestionView ()

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (copy, nonatomic) AIRSetupBlock setupBlock;

@property (strong, nonatomic) NSArray *answersContainer;

@end

@implementation AIRFinishQuestionView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupNib];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(setupAnswers:)
                                                     name:kSearchParams
                                                   object:nil];
    }
    return self;
}

- (void)setupAnswers:(NSNotification *)notification {
    
    NSArray *answers;
    if ([notification.name isEqualToString:kSearchParams]) {
        NSDictionary *userInfo = notification.userInfo;
        answers = (NSArray *)userInfo[@"answers"];
    }
    
    self.answersContainer = answers;
}

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRFinishQuestionView.class) owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = ScreenBounds;
}

- (void)updateConstraints {
    
     if (IS_IPHONE_5_OR_LESS) {
         [self.viewContainer makeConstraints:^(MASConstraintMaker *make) {
             make.left.equalTo(self.mas_left);
             make.right.equalTo(self.mas_right);
             make.centerX.equalTo(self.mas_centerX);
             make.centerY.equalTo(self.mas_centerY).multipliedBy(0.9);
         }];
     } else {
         [self.viewContainer makeConstraints:^(MASConstraintMaker *make) {
             make.left.equalTo(@0);
             make.right.equalTo(@0);
             make.centerX.equalTo(self.mas_centerX);
             make.centerY.equalTo(self.mas_centerY).multipliedBy(0.9);
         }];
     }
    
    [super updateConstraints];
}

#pragma mark - Events

- (IBAction)showHomesPressed:(id)sender {
    
    [[AIRAPIManager sharedAPI]
     saveWithSearchParamsWithAnswers:self.answersContainer
     callback:^(AIRAccount *account, NSError *error) {
         
         if (self.setupBlock) {
             self.setupBlock(YES);
         }
     }];
}

- (void)answerWithCompletion:(AIRSetupBlock)completion {
    self.setupBlock = completion;
}

@end

//
//  AIRPhonePasswordView.h
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIRPhonePasswordView : UIView

typedef void (^AIRAnswerBlock)(NSString *phone, NSString *password);

- (void)answerWithCompletion:(AIRAnswerBlock)completion;

@end

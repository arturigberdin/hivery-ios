//
//  AIRPhonePasswordView.m
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRPhonePasswordView.h"

//Libs
#import "NBPhoneNumberUtil.h"

#define kPhoneFieldTag 101

@interface AIRPhonePasswordView () <UITextFieldDelegate> {
    NSInteger cursorPosition;
}

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *phoneContainer;
@property (weak, nonatomic) IBOutlet UIView *passwordContainer;

@property (copy, nonatomic) AIRAnswerBlock answerBlock;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) NSString *phoneNumber;

@end

@implementation AIRPhonePasswordView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupNib];
        self.phoneTextField.delegate = self;
        self.passwordTextField.delegate = self;
        self.phoneTextField.tag = kPhoneFieldTag;
        
        self.passwordTextField.text = @"12345";
    }
    return self;
}

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRPhonePasswordView.class) owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = ScreenBounds;
}

- (void)updateConstraints {
    
    if (IS_IPHONE_5_OR_LESS) {
        [self.phoneContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@5);
            make.right.equalTo(@5);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.715); //515
        }];
        
        [self.passwordContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@5);
            make.right.equalTo(@5);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(1.0);
        }];
    } else {
        
        [self.phoneContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@20);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.725); //525
        }];
        
        [self.passwordContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@20);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(1.0);
        }];
    }
    [super updateConstraints];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSString *cleanPhone = [self cleanPhone:self.phoneTextField.text];
    
    BOOL validPhone = [AIRValidator validatePhone:cleanPhone];
    BOOL validPassword = [AIRValidator validatePassword:self.passwordTextField.text];

    if ([self.phoneTextField isFirstResponder]) {
        if (validPhone) {
            [self.phoneTextField becomeFirstResponder];
        }
    }
    
    if (validPhone && validPassword) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.answerBlock) {
                self.answerBlock(cleanPhone, self.passwordTextField.text);
            }
        });
    }
    else {
        
        NSMutableString *alertString = [NSMutableString new];
        
        NSString *phone = [self.phoneTextField.text trimmingWhitespaces];
        NSString *password = [self.passwordTextField.text trimmingWhitespaces];
        
        //text was empty or only whitespace
        if (phone.length == 0) {
            [alertString appendString:@"Phone is required"];
        } else if (!validPhone) {
            [alertString appendString:@"Invalid phone"];
        }
        
        if (password.length == 0) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Password is required"];
        } else if (!validPassword) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Invalid password \n (contain from 5 to 15 characters)"];
        }
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:alertString message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        [alert show];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == kPhoneFieldTag) {
        NSMutableString *mutablePhone = NSMutableString.new;
        [mutablePhone appendString:textField.text];
        [mutablePhone appendString:string];
        
        NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
        NSError *anError = nil;
        NBPhoneNumber *myNumber = [phoneUtil parseWithPhoneCarrierRegion:mutablePhone error:&anError];
        
        NSString *resultNumber;
        
        resultNumber = [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:&anError];
        
        if(resultNumber) {
            [textField setText:resultNumber];
        } else {
            [textField setText:mutablePhone];
        }
        
        NSLog(@"INTERNATIONAL : %@", [phoneUtil format:myNumber
                                          numberFormat:NBEPhoneNumberFormatINTERNATIONAL
                                                 error:&anError]);
        //backspace pressed
        if (range.length==1 && string.length==0) return YES;
        
        return NO;
    }
    return YES;
}

#pragma mark - Entry point

- (void)answerWithCompletion:(AIRAnswerBlock)completion {
    self.answerBlock = completion;
}

#pragma mark - Private

- (NSString *)cleanPhone:(NSString *)phone {
    NSString *resultPhone = phone;
    resultPhone = [resultPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    resultPhone = [resultPhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    return resultPhone;
}

@end

//
//  AIRQuestionsViewController+UI.m
//  hivery
//
//  Created by Artur on 14/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRSetUpProfileVC+UI.h"

@interface AIRSetUpProfileVC ()

@end

@implementation AIRSetUpProfileVC (UI)

@dynamic backButton;
@dynamic setupYourProfileLabel;

#pragma mark - UI

- (void)showNavigationControls {
    [self.view bringSubviewToFront:self.backButton];
    [self.view bringSubviewToFront:self.setupYourProfileLabel];
}

- (void)hideNavigationControls {
    [self.view sendSubviewToBack:self.backButton];
    [self.view sendSubviewToBack:self.setupYourProfileLabel];
}


@end

//
//  AIRBedroomQuestionView.h
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

//Models
#import "AIRAccount.h"

@interface AIRBedroomQuestionView : UIView

typedef void (^AIRBedroomBlock)(NSInteger bedroom);

- (void)answerWithCompletion:(AIRBedroomBlock)completion;

//Entry point
- (void)setupWithAccount:(AIRAccount *)account;

@end

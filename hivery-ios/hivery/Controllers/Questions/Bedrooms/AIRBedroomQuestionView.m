//
//  AIRBedroomQuestionView.m
//  hivery
//
//  Created by Artur on 01/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRBedroomQuestionView.h"

@interface AIRBedroomQuestionView ()

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *questionContainer;

@property (weak, nonatomic) IBOutlet UIView *bedroomContainer;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;

@property (copy, nonatomic) AIRBedroomBlock bedroomBlock;

@end

@implementation AIRBedroomQuestionView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupNib];
        [self setupButtons];
    }
    return self;
}

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRBedroomQuestionView.class) owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = ScreenBounds;
}

- (void)setupButtons {
    [self.button1 setImage:[UIImage imageNamed:@"Bedroom1"] forState:UIControlStateNormal];
    [self.button1 setImage:[UIImage imageNamed:@"BedroomSelected1"] forState:UIControlStateSelected];
    self.button1.tag = 1;
    [self.button1 addTarget:self action:@selector(toggleSelection:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.button2 setImage:[UIImage imageNamed:@"Bedroom2"] forState:UIControlStateNormal];
    [self.button2 setImage:[UIImage imageNamed:@"BedroomSelected2"] forState:UIControlStateSelected];
    self.button2.tag = 2;
    [self.button2 addTarget:self action:@selector(toggleSelection:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.button3 setImage:[UIImage imageNamed:@"Bedroom3"] forState:UIControlStateNormal];
    [self.button3 setImage:[UIImage imageNamed:@"BedroomSelected3"] forState:UIControlStateSelected];
    self.button3.tag = 3;
    [self.button3 addTarget:self action:@selector(toggleSelection:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)toggleSelection:(UIButton *)sender {
    [self deselectButtons];
    
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    NSInteger bedroom = (NSInteger)sender.tag;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (self.bedroomBlock) {
            self.bedroomBlock(bedroom);
        }
    });
}

- (void)deselectButtons {
    self.button1.selected = NO;
    self.button2.selected = NO;
    self.button3.selected = NO;
}

- (void)setupWithAccount:(AIRAccount *)account {
    
    [self deselectButtons];
    
    NSInteger bedrooms;
    if (account.bedroomsContainer.count > 0) {
        bedrooms = [account.bedroomsContainer[0] integerValue];
        
        if (bedrooms == 1) {
            self.button1.selected = YES;
        } else if (bedrooms == 2) {
            self.button2.selected = YES;
        } else if (bedrooms == 3) {
            self.button3.selected = YES;
        }
    }
}

- (void)layoutSubviews {
    
    if (IS_IPHONE_5_OR_LESS) {
        [self.questionContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.75);
        }];
        [self.bedroomContainer makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            //make.bottom.equalTo(@30);
        }];
    } else {
        [self.questionContainer makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY).multipliedBy(0.75);
        }];
    }
    
    [super layoutSubviews];
}

- (void)answerWithCompletion:(AIRBedroomBlock)completion {
    self.bedroomBlock = completion;
}

@end

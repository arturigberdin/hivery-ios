//
//  AIREditProfileCell.h
//  hivery
//
//  Created by Artur on 28/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIREditProfileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *profileLabel;
@property (weak, nonatomic) IBOutlet UITextField *profileTextField;

@end

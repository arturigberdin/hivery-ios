//
//  AIRProfileCell.h
//  hivery
//
//  Created by Artur on 20/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIRProfileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel;

@property (weak, nonatomic) IBOutlet UIButton *arrowButton;

@end

//
//  AIREditProfileViewController.m
//  hivery
//
//  Created by Artur on 27/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIREditProfileViewController.h"

//Cells
#import "AIREditProfileCell.h"

//API
#import "AIRAPIManager.h"

//Models
#import "AIRAccount.h"


@interface AIREditProfileViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *profileArray;

@property (strong, nonatomic) NSMutableArray *profileContainer;

@property (strong, nonatomic) AIRAccount *account;

@end

@implementation AIREditProfileViewController

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[AIRAPIManager sharedAPI] getInfoWithCallback:^(AIRAccount *account, NSError *error) {
        
        if (account) {
            self.profileContainer = [@[account.name, account.email, account.phone] mutableCopy];

            self.account = account;
            
            [self.tableView reloadData];
        }
        
    }];
    
    // This will remove extra separators from tableview
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.tableView.tableFooterView.backgroundColor = [UIColor clearColor];
    
    self.profileArray = @[@"Name", @"Email", @"Mobile number"]; //, @"Occupation", @"Company name"];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackArrow"]
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonTap)];
    self.navigationItem.leftBarButtonItem = cancelButton;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.profileArray.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AIREditProfileCell";
    
    AIREditProfileCell *cell = (AIREditProfileCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AIREditProfileCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (indexPath.row == self.profileArray.count) {
        cell.profileTextField.hidden = YES;
        cell.profileLabel.hidden = YES;
        
        return cell;
    }
    
    cell.profileLabel.text = self.profileArray[indexPath.row];
    cell.profileTextField.placeholder = self.profileArray[indexPath.row];
    cell.profileTextField.text = self.profileContainer[indexPath.row];
    
    if (indexPath.row == 0) {
        cell.profileTextField.tag = 100;
    }
    if (indexPath.row == 1) {
        cell.profileTextField.tag = 101;
    }
    
    if (indexPath.row == 2) cell.profileTextField.enabled = NO;
    
    //Hide last line of separator
    if (indexPath.row == self.profileArray.count) {
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

#pragma mark - Footer (Separator problem)

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

#pragma mark - Events

- (void)cancelButtonTap {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    UITextField *nameTextField = (UITextField *)[cell viewWithTag:100];

    indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    cell = [self.tableView cellForRowAtIndexPath:indexPath];
    UITextField *emailTextField = (UITextField *)[cell viewWithTag:101];
    
    
    if (nameTextField.text.length == 0 && emailTextField.text.length == 0) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    BOOL validEmail = [AIRValidator validateEmail:emailTextField.text];
    BOOL validName = [AIRValidator validateName:nameTextField.text];
    
    if (validName && validEmail) {
        
        if (![self.account.name isEqualToString: nameTextField.text] ||
            ![self.account.email isEqualToString: emailTextField.text]) {
            
            DLog(@"Profile info changed!");
            
            NSString *alertString = @"Do you want to change account information?";
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@"Account" message:alertString preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [[AIRAPIManager sharedAPI] setInfoWithUsername:nameTextField.text
                                                         email:emailTextField.text
                                                    occupation:@""
                                                   companyName:@""
                                                      callback:^(AIRAccount *account, NSError *error) {
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                      }];
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }]];
            [alert show];
            
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } else {
        NSMutableString *alertString = [NSMutableString new];
        
        NSString *name = nameTextField.text;
        NSString *email = emailTextField.text;
        name = [name trimmingWhitespaces];
        email = [email trimmingWhitespaces];
        
        //text was empty or only whitespace
        if (name.length == 0) {
            [alertString appendString:@"Name is required"];
        } else if (!validName) {
            [alertString appendString:@"Invalid name"];
        }
        
        if (email.length == 0) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Email is required"];
        } else if (!validEmail) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Invalid email"];
        }

        [AIRAlert alertWithTitle:@"Validation Status" message:alertString];
    }
}

@end

//
//  AIRProfileViewController.h
//  hivery
//
//  Created by Artur on 08/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIRProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

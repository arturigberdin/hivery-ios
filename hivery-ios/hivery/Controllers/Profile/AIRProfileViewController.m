//
//  AIRProfileViewController.m
//  hivery
//
//  Created by Artur on 08/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRProfileViewController.h"

//Controllers
#import "AIRMainViewController.h"
#import "AIREditProfileViewController.h"
#import "AIRBedroomsViewController.h"
#import "AIRDistrictsViewController.h"

//App Delegate
#import "AppDelegate.h"

//API
#import "AIRAPIManager.h"

//Models
#import "AIRAccount.h"

//Cells
#import "AIRProfileCell.h"

@interface AIRProfileViewController ()<UITableViewDataSource, UITableViewDelegate>

//Controls
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

//Content
@property (strong, nonatomic) NSMutableArray *profileOptions;

//App Delegate
@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) AIRAccount *account;
@property (weak, nonatomic) IBOutlet UIImageView *profileThemeImageView;

@end

@implementation AIRProfileViewController

@synthesize appDelegate;

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = @"Profile";
    
    appDelegate =  [[UIApplication sharedApplication] delegate];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.profileOptions = [@[@"Edit profile",
                            @"Edit bedrooms",
                            @"Edit areas you're interested in",
                            @"Log out"] mutableCopy];
    
    // This will remove extra separators from tableview
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [[AIRAPIManager sharedAPI] getInfoWithCallback:^(AIRAccount *account, NSError *error) {
        if (account) {
            self.account = account;
            self.nameLabel.text = StringConcat(account.name,@"!");
        }
    }];
}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
    [self.profileThemeImageView makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(ScreenHeight/4));
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.profileOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"AIRProfileCell";
    
    AIRProfileCell *cell = (AIRProfileCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AIRProfileCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.mainTitleLabel.text = self.profileOptions[indexPath.row];
    
    cell.arrowButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    
    NSInteger optionIndex = indexPath.row + 1;
    if (optionIndex == 4) {
        cell.arrowButton.hidden = YES;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ScreenHeight/9;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row + 1 == 1) {
        [self showEditProfileController];
    }
    
    if (indexPath.row + 1 == 2) {
        [self showBedroomsController];
    }
    
    if (indexPath.row + 1 == 3) {
        [self showDistrictsController];
    }
    
    //Login
    if (indexPath.row + 1 == 4) {
        [self logoutFromAlert];
    }
}

#pragma mark - Footer (Separator problem)

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

#pragma mark - Actions

- (void)logoutFromAlert {
    
    NSString *alertTitle = @"Logout";
    NSString *alertMessage = @"Are you sure you want to logout?";
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self logout];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    [alert show];
}

- (void)logout {
    
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:kPhoneNumber];
    //[[NSUserDefaults standardUserDefaults] setObject:nil forKey:kChangeAccessToken];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUserAuthorized];
    
    [[AIRAPIManager sharedAPI] removeDeviceTokenWithCallback:^(BOOL complete, NSError *error) {
        if (complete) {
            DLog(@"removeDeviceTokenWithCallback");
        }
    }];
    
    [self showMainController];
}

#pragma mark - Navigation

- (void)showMainController {
    [appDelegate showMainController];
}

- (void)showEditProfileController {
    AIREditProfileViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIREditProfileViewController.class)];
    
    vc.title = @"Edit Profile";
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showBedroomsController {
    AIRBedroomsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRBedroomsViewController.class)];
    
    vc.title = @"Bedrooms";
    vc.account = self.account;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showDistrictsController {
    AIRDistrictsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRDistrictsViewController.class)];
    
    vc.title = @"Areas";
    vc.account = self.account;
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end

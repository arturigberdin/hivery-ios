//
//  AIRProfileCell.m
//  hivery
//
//  Created by Artur on 20/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRProfileCell.h"

@implementation AIRProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

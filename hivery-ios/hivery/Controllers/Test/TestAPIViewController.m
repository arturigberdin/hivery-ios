//
//  ViewController.m
//  hivery
//
//  Created by Artur on 15/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "TestAPIViewController.h"

//API
#import "AIRAPIManager.h"

//Security
#import <SAMKeychain/SAMKeychain.h>

//Models
#import "AIRAccessToken.h"

@interface TestAPIViewController ()

@end

@implementation TestAPIViewController

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    DLog(@"Screen Bounds = %@", NSStringFromCGRect(ScreenBounds));
    
    //NSString *localizationString = NSLocalizedString(@"Hivery", nil);
    //self.testLabel.text = localizationString;
}

#pragma mark - Actions

- (void)authorizeWithPhone {
    
    NSString *phone = @"+79871892044";
    
    [[AIRAPIManager sharedAPI]
     authorizeWithPhone:phone
     password:@"12345"
     callback:^(NSString *accessToken, NSError *error) {
         
         BOOL result = [SAMKeychain setPassword:accessToken forService:@"com.hivery.token" account:phone];
         if (result) {
             NSString *token = [SAMKeychain passwordForService:@"com.hivery.token" account:phone];
             DLog(@"token = %@",token);
             [[NSNotificationCenter defaultCenter] postNotificationName:kChangeAccessToken object:token];
          }
     }];
}

- (void)authorizeWithEmail {
    [[AIRAPIManager sharedAPI]
     authorizeWithEmail:@"art.igberdin@gmail.com"
     password:@"12345"
     callback:^(NSString *accessToken, NSError *error) {
         
     }];
}

- (void)registerAccount {
    
    NSString *username = @"Arthur";
    NSString *email = @"art.igberdin@gmail.com";
    NSString *phone = @"+79871892044";
    NSString *password = @"12345";
    
    [[AIRAPIManager sharedAPI]
     registerWithUsername:username
     email:email
     phone:phone
     password:password
     callback:^(NSString *token, NSError *error) {
        
    }];
}

- (void)restorePasscode {
    
    [[AIRAPIManager sharedAPI]
     restoreVerificationCodeWithPhone:@"+79871892044"
     callback:^(NSString *verificationCode, NSError *error) {
         
     }];
}

- (void)getSearchParams {
    
    [[AIRAPIManager sharedAPI]
     getSearchParamsWithCallback:^(NSArray *searchParams, NSError *error) {
         
     }];
}

#pragma mark - Events

//auth.token (phone)
- (IBAction)authorizeWithPhonePressed:(id)sender {
    [self authorizeWithPhone];
}

//auth.token (email)
- (IBAction)authorizeWithEmailPressed:(id)sender {
    [self authorizeWithEmail];
}

//account.register
- (IBAction)registerAccountPressed:(id)sender {
    [self registerAccount];
}

//auth.restorePasscode
- (IBAction)restorePasscodePressed:(id)sender {
    [self restorePasscode];
}

//questions.get (deprecated!)
- (IBAction)getSearchParamsPressed:(id)sender {
    DLog(@"questins.get (deprecated!)");
    [self getSearchParams];
}


- (IBAction)saveSearchParams:(id)sender {
    
//    [[AIRAPIManager sharedAPI]
//     t_saveSearchParamsWithAnswers:@""
//     callback:^(AIRAccount *account, NSError *error){
//         
//         DLog(@"t_saveSearchParamsWithAnswers");
//     }];
}

//account.getInfo
- (IBAction)getInfoPressed:(id)sender {
    
    [[AIRAPIManager sharedAPI] getInfoWithCallback:^(AIRAccount *account, NSError *error) {
        DLog(@"setInfoWithCallback");
    }];
}

//account.setInfo
- (IBAction)setInfoPressed:(id)sender {
    
    [[AIRAPIManager sharedAPI] setInfoWithCallback:^(AIRAccount *account, NSError *error) {
        DLog(@"setInfoWithCallback");
    }];
}

//estates.get
- (IBAction)getEstates:(id)sender {
    
    [[AIRAPIManager sharedAPI] getEstatesWithCallback:^(NSArray *estates, NSError *error) {
        DLog(@"estates = %@", estates);
    }];
}

//estate.choice
- (IBAction)choiceEstates:(id)sender {

    [[AIRAPIManager sharedAPI]
     choiceEstate:52
     reaction:@"dislike"
     callback:^(BOOL complete, NSError *error) {
         
         
     }];
}

@end

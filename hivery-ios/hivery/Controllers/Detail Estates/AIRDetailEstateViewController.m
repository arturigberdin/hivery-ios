//
//  DetailEstateViewController.m
//  hivery
//
//  Created by Artur on 25/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRDetailEstateViewController.h"

//API
#import "AIRAPIManager.h"
#import "UIImageView+AFNetworking.h"

//Models
#import "AIREstate.h"

//Controllers
//#import "AIRMakeBidViewController.h"

@interface AIRDetailEstateViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *districtLabel;
@property (weak, nonatomic) IBOutlet UILabel *bedroomsLabel;
@property (weak, nonatomic) IBOutlet UILabel *bathroomsLabel;

@property (weak, nonatomic) IBOutlet UIView *priceViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIView *verticalBorderView;

@property (weak, nonatomic) IBOutlet UIButton *negativeButton;
@property (weak, nonatomic) IBOutlet UIButton *positiveButton;

@property (assign, nonatomic) BOOL isFromLikes;

//Content
@property (strong, nonatomic) AIREstate *estate;

//Scroll images
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation AIRDetailEstateViewController

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (_isFromLikes) {
        [self.negativeButton updateConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.bottom.equalTo(@0);
            make.height.equalTo(@55);
        }];
        
        [self.negativeButton setTitle:@"Remove from likes" forState:UIControlStateNormal];
        self.verticalBorderView.hidden = YES;
        self.positiveButton.hidden = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    //Constraints with animation can be here...
}

#pragma mark - Entry point

- (void)setupWithEstateFromFeed:(AIREstate *)estate {
  
    self.estate = estate;
    
    self.nameLabel.text = estate.name;
    
    self.addressLabel.text = estate.address;
    self.districtLabel.text = [NSString stringWithFormat:@"District %@", estate.district];
    
    self.bedroomsLabel.text = [NSString stringWithFormat:@"%ld", (long)estate.bedsCount];
    self.bathroomsLabel.text = [NSString stringWithFormat:@"%ld", (long)estate.bathsCount];
    
    self.priceLabel.text = [NSString stringWithFormat:@"$%@", estate.closingPrice];
    
    if (!estate.closingPrice) self.priceViewContainer.hidden = YES;
    
    if (self.estate.images.count > 0) {
        [self setupImages:self.estate.images];
    }
    
    [self setupImages:self.estate.images];
}

- (void)setupWithEstateFromLikes:(AIREstate *)estate {

    self.isFromLikes = YES;
    [self setupWithEstateFromFeed:estate];
}

- (void)setupImages:(NSArray *)images {
    
    CGFloat imageHeight = ScreenHeight/2;
    
    self.scrollView.delegate = self;
    
    int index = 0;
    for (NSString *urlString in images) {
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame = CGRectMake(index * ScreenWidth, 0, ScreenWidth, imageHeight);
        
        [imageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"Apartment"]];
        
        [self.scrollView addSubview:imageView];
        
        DLog(@"url = %@", urlString);
        
        index ++;
    }
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.scrollEnabled = YES;
    
    self.scrollView.contentSize = CGSizeMake(ScreenWidth * images.count, imageHeight);
    
    self.pageControl.numberOfPages = images.count;
    self.pageControl.currentPage = 0;
    self.pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0xEE4054);
    self.pageControl.userInteractionEnabled = NO;
    [self.view addSubview:self.pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}

#pragma mark - Events

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)noThanksButtonPressed:(id)sender {
    
    if (_isFromLikes) {
        [[AIRAPIManager sharedAPI]
         choiceEstate:self.estate.estateID
         reaction:@"dislike"
         callback:^(BOOL complete, NSError *error) {
             NSLog(@"likeItButtonPressed");
         }];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)likeItButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

//
//  DetailEstateViewController.h
//  hivery
//
//  Created by Artur on 25/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

//Models
#import "AIREstate.h"

@interface AIRDetailEstateViewController : UIViewController

//Entry points
- (void)setupWithEstateFromFeed:(AIREstate *)estate;
- (void)setupWithEstateFromLikes:(AIREstate *)estate;

@end

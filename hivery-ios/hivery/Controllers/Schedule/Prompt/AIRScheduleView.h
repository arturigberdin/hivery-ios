//
//  AIRScheduleView.h
//  hivery
//
//  Created by Artur on 19/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AIRClosePopupBlock)(BOOL isClosed);

@interface AIRScheduleView : UIView

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *scheduleView;

- (instancetype)initWithCompletion:(AIRClosePopupBlock)completion;

- (void)displayPopup;
- (void)hidePopup;

@end

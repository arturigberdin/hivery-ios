//
//  AIRScheduleView.m
//  hivery
//
//  Created by Artur on 19/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRScheduleView.h"

@interface AIRScheduleView ()

@property (copy, nonatomic) AIRClosePopupBlock popupBlock;

@end

@implementation AIRScheduleView

#pragma mark - Initialize

//Override if View call init
- (instancetype)initWithCompletion:(AIRClosePopupBlock)completion {
    self = [super init];
    if (self) {
        [self setupNib];
        
        self.popupBlock = completion;
    }
    return self;
}

- (void)setupNib {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRScheduleView.class) owner:self options:nil];
    [self addSubview:self.view];
    
    self.view.frame = ScreenBounds;
}

#pragma mark - Entry point

- (IBAction)closePopupPressed:(id)sender {
    if (self.popupBlock) {
        self.popupBlock(YES);
    }
}

- (void)displayPopup {
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:self.view];
}

- (void)hidePopup {
    [self.view removeFromSuperview];
}

@end

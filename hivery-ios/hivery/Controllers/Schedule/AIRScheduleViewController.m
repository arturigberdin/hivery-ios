//
//  AIRScheduleViewController.m
//  hivery
//
//  Created by Artur on 08/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRScheduleViewController.h"

//API
#import "AIRAPIManager.h"

//Cells
#import "AIRScheduleCell.h"

//Views
#import "AIRScheduleView.h"

//Controllers
#import "AIRDetailEstateViewController.h"

@interface AIRScheduleViewController () <UITableViewDataSource, UITableViewDelegate>

//Views
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//Controls
@property (weak, nonatomic) IBOutlet UILabel *scheduleTimeLabel;
@property (strong, nonatomic) AIRScheduleView *scheduleView;

//Content
@property (strong, nonatomic) NSMutableArray *estates;

@end

@implementation AIRScheduleViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Estates you liked";
    
    self.estates = [NSMutableArray new];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    //Footer use for non-displaying separators in empty cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [[AIRAPIManager sharedAPI] scheduleEstatesWithCallback:^(NSArray *estates, NSError *error) {
        
        self.estates = [estates mutableCopy];
        [self.tableView reloadData];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.scheduleTimeLabel.text = [self takeNextSunday];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.estates.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"AIRScheduleCell";
    
    AIRScheduleCell *cell = (AIRScheduleCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AIRScheduleCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell configureWithEstate:self.estates[indexPath.row] index:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return ScreenHeight/5;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AIRScheduleCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    [self showDetailEstateControllerWith:cell.estate];
}

- (void)showDetailEstateControllerWith:(AIREstate *)estate {
    
    UIStoryboard *sb =  [UIStoryboard storyboardWithName:@"DetailEstate" bundle:nil];
    
    AIRDetailEstateViewController *detailVC = [sb instantiateViewControllerWithIdentifier:NSStringFromClass(AIRDetailEstateViewController.class)];

    [self presentViewController:detailVC animated:YES completion:nil];
    
    [detailVC setupWithEstateFromLikes:estate];
}


#pragma mark - Actions

- (NSString *)takeNextSunday {
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *date = [NSDate date];
    NSDateComponents *weekdayComponents = [gregorian components:NSCalendarUnitWeekday fromDate:date];
    NSInteger todayWeekday = [weekdayComponents weekday];
    
    enum Weeks {
        SUNDAY = 1,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY
    };
    
    NSInteger moveDays = SUNDAY - todayWeekday;
    if (moveDays <= 0) {
        moveDays += 7;
    }
    
    NSDateComponents *components = [NSDateComponents new];
    components.day = moveDays;
    
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
    
    NSLog(@"%@",newDate);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE d LLL"]; //SUN 25 SEN
    NSLog(@"%@", [dateFormatter stringFromDate:newDate]);
    NSString *dateString = [dateFormatter stringFromDate:newDate];
    
    return [dateString uppercaseString];
}

#pragma mark - Events

- (IBAction)howDoesItWorkPressed:(id)sender {
    DLog(@"howDoesItWorkPressed");
    
    self.scheduleView = [[AIRScheduleView alloc]
                         initWithCompletion:^(BOOL isClosed) {
                             
                             if (isClosed) {
                                 [self.scheduleView hidePopup];
                             }
                         }];
    
    [self.scheduleView displayPopup];
}

#pragma mark - Footers (Solve separator problem)

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

@end

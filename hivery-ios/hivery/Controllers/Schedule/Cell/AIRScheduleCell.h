//
//  AIRScheduleCell.h
//  hivery
//
//  Created by Artur on 16/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AIREstate;

@interface AIRScheduleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *estateImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *districtLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIView *priceContainerView;

@property (strong, nonatomic) AIREstate *estate;


- (void)configureWithEstate:(AIREstate *)estate index:(NSInteger)index;

@end

//
//  AIRScheduleCell.m
//  hivery
//
//  Created by Artur on 16/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRScheduleCell.h"

//Models
#import "AIREstate.h"

#import "UIImageView+AFNetworking.h"

@implementation AIRScheduleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self addConstraint:
     [NSLayoutConstraint constraintWithItem:self.estateImageView
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0
                                   constant:ScreenWidth/2.5]];
    
    [self.nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.estateImageView.mas_right).with.offset(@20);
    }];
    
    [self.addressLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.estateImageView.mas_right).with.offset(@20);
    }];
    
    [self.districtLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.estateImageView.mas_right).with.offset(@20);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureWithEstate:(AIREstate *)estate index:(NSInteger)index {
    
    self.estate = estate;

    NSURL *url;
    if (estate.images.count > 0) url = [NSURL URLWithString:estate.images[0]];
    [self.estateImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Apartment"]];
    
    self.nameLabel.text = estate.name;
    self.addressLabel.text = estate.address;
    self.districtLabel.text = [NSString stringWithFormat:@"%@", estate.district];
    
    self.priceLabel.text = [NSString stringWithFormat:@"$%@", estate.closingPrice];
    if (!estate.closingPrice) {
        self.priceContainerView.hidden = YES;
    }
}

- (UIEdgeInsets)layoutMargins {
    return UIEdgeInsetsZero;
}

@end

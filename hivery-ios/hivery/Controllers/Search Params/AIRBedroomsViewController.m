//
//  AIRBedroomsViewController.m
//  hivery
//
//  Created by Artur on 29/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRBedroomsViewController.h"

//Views
#import "AIRBedroomQuestionView.h"

//Models
#import "AIRAnswer.h"
#import "AIRAccount.h"

//API
#import "AIRAPIManager.h"

@interface AIRBedroomsViewController ()

@property (strong, nonatomic) NSMutableArray *answersContainer;

@end

@implementation AIRBedroomsViewController

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.answersContainer = [NSMutableArray new];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackArrow"]
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    [self setupUI];
}

- (void)setupUI {
    AIRBedroomQuestionView *bedroomView = [[AIRBedroomQuestionView alloc] init];
    bedroomView.frame = CGRectMake(0, -ScreenHeight/10, ScreenWidth, ScreenHeight+ScreenHeight/10);
    
    [bedroomView answerWithCompletion:^(NSInteger bedroom) {
        
        [self.answersContainer removeAllObjects];
        
        AIRAnswer *answer = [[AIRAnswer alloc] init];
        answer.questionID = 17; //bedrooms params
        answer.answers = @[@(bedroom)];
        [self.answersContainer addObject:answer];
        
    }];
    
    [bedroomView setupWithAccount:self.account];
    
    [self.view addSubview:bedroomView];
}

#pragma mark - Events

- (void)cancelButtonPressed {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [[AIRAPIManager sharedAPI]
     saveWithSearchParamsWithAnswers:self.answersContainer
     callback:^(AIRAccount *account, NSError *error) {
         
     }];
}

@end

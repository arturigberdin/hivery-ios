//
//  AIRDistrictsViewController.m
//  hivery
//
//  Created by Artur on 29/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRDistrictsViewController.h"

//View
#import "AIRAreasQuestionView.h"

//Models
#import "AIRAnswer.h"
#import "AIRAccount.h"

//API
#import "AIRAPIManager.h"

@interface AIRDistrictsViewController ()

@property (strong, nonatomic) NSMutableArray *answersContainer;

@property (strong, nonatomic) AIRAreasQuestionView *areasView;

@end

@implementation AIRDistrictsViewController

@synthesize areasView;

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.answersContainer = [NSMutableArray new];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackArrow"]
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancelButtonPressed)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    [self setupUI];
}

- (void)setupUI {
    
    areasView = [[AIRAreasQuestionView alloc] init];
    areasView.frame = CGRectMake(0, -ScreenHeight/10, ScreenWidth, ScreenHeight+ScreenHeight/10);
    
    [self.view addSubview:areasView];
    
    [areasView answerWithCompletion:^(NSArray *options) {
        
        NSMutableArray *container = [NSMutableArray new];
        AIRAnswer *answer = [[AIRAnswer alloc] init];
        answer.questionID = 30; //district params
        for (NSString *option in options) {
            [container addObject:option];
        }
        
        answer.answers = [container copy];
        
        [self.answersContainer addObject:answer];
        
        [self.navigationController popViewControllerAnimated:YES];
        
        [[AIRAPIManager sharedAPI]
         saveWithSearchParamsWithAnswers:self.answersContainer
         callback:^(AIRAccount *account, NSError *error) {
             
             
         }];
        
    }];
    
    [areasView setupWithAccount:self.account];
}

#pragma mark - Events

- (void)cancelButtonPressed {
    
    [areasView sendAnswerBlock];
}


@end

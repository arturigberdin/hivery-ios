//
//  AIRDistrictsViewController.h
//  hivery
//
//  Created by Artur on 29/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AIRAccount;

@interface AIRDistrictsViewController : UIViewController

@property (strong, nonatomic) AIRAccount *account;

@end

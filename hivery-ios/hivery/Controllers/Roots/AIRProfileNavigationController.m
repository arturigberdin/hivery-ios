//
//  AIRProfileNavigationController.m
//  hivery
//
//  Created by Artur on 28/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRProfileNavigationController.h"

@interface AIRProfileNavigationController ()

@end

@implementation AIRProfileNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationBar.barTintColor = UIColorFromRGB(0xF7F7F8);
    self.navigationBar.tintColor = UIColorFromRGB(0x667190);
    self.navigationBar.translucent = NO;

    //self.navigationBar.clipsToBounds = YES;
    
    //[[UINavigationBar appearance] setBarTintColor:[UIColor yellowColor]];
    //self.navigationBar.translucent = NO;
    
    // Remove navigation bar 1 px bottom line
    
    
    NSDictionary *attributes = @{ //NSBackgroundColorAttributeName : [UIColor yellowColor],
                                  NSFontAttributeName : [UIFont fontWithName:@"GothamRounded-Book" size:16],
                                  NSForegroundColorAttributeName : UIColorFromRGB(0x667190)
                                  };
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

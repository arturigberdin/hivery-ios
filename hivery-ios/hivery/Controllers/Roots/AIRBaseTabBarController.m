//
//  AIRBaseTabBarController.m
//  hivery
//
//  Created by Artur on 07/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRBaseTabBarController.h"

@implementation AIRBaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTabBar];
}

- (void)configureTabBar {
    
    UITabBar *tabBar = self.tabBar;
    UITabBarItem *item0 = [tabBar.items objectAtIndex:0];
    UITabBarItem *item1 = [tabBar.items objectAtIndex:1];
    UITabBarItem *item2 = [tabBar.items objectAtIndex:2];

    //SELECTED COLOR
    self.tabBar.tintColor = [UIColor whiteColor];
    
    //BACKGROUND
    UIImage* tabBarBackground = [UIImage imageWithUIColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    
    //SHADOW
    [[UITabBar appearance] setShadowImage:[UIImage imageWithUIColor:[UIColor clearColor]]];

    [item0 setImage:[[self imageWithImage:[UIImage imageNamed:@"Profile"] scaledToSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item0 setSelectedImage:[[self imageWithImage:[UIImage imageNamed:@"ProfileSelected"] scaledToSize:CGSizeMake(26, 26)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    item0.tag = 0;
    
    [item1 setImage:[[self imageWithImage:[UIImage imageNamed:@"Cards"] scaledToSize:CGSizeMake(29, 29)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item1 setSelectedImage:[[self imageWithImage:[UIImage imageNamed:@"CardsSelected"] scaledToSize:CGSizeMake(30, 30)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    item1.tag = 1;
    
    [item2 setImage:[[self imageWithImage:[UIImage imageNamed:@"Schedule"] scaledToSize:CGSizeMake(24, 24)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [item2 setSelectedImage:[[self imageWithImage:[UIImage imageNamed:@"ScheduleSelected"] scaledToSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    item2.tag = 2;
    
    //IMAGE INSETS
    if (IS_IPHONE_5_OR_LESS) {
        item0.imageInsets = UIEdgeInsetsMake(5, 20, -5, -20);
        item1.imageInsets = UIEdgeInsetsMake(4, 0, -4, 0);
        item2.imageInsets = UIEdgeInsetsMake(5, -20, -5, 20);
    } else {
        item0.imageInsets = UIEdgeInsetsMake(5, 25, -5, -25);
        item1.imageInsets = UIEdgeInsetsMake(4, 0, -4, 0);
        item2.imageInsets = UIEdgeInsetsMake(5, -25, -5, 25);
    }
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Events

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {

    DLog(@"tab selected: %ld", (long)item.tag);
    
    //Cards Tab pressed
    if(item.tag == 1) {
        
    }
}

@end

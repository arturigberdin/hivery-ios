//
//  AIRLoginViewController.m
//  hivery
//
//  Created by Artur on 23/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRLoginViewController.h"

//Libs
#import "NBPhoneNumberUtil.h"

//API
#import "AIRAPIManager.h"

//Categories
#import "UIViewController+Navigation.h"

#define kPhoneFieldTag 101

@interface AIRLoginViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *phoneViewContainer;
@property (weak, nonatomic) IBOutlet UIView *codeViewContainer;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeTextField;

@property (copy, nonatomic) NSString *password;

@end

@implementation AIRLoginViewController

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.phoneTextField.delegate = self;
    self.verificationCodeTextField.delegate = self;
    
    self.password = @"12345";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Events

- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginButtonPressed:(id)sender {
    
    NSString *cleanPhone = [self cleanPhone:self.phoneTextField.text];
    BOOL validPhone = [AIRValidator validatePhone:cleanPhone];
    BOOL validVerificationCode = [AIRValidator validateVerificationCode:self.verificationCodeTextField.text];
    
    if (validPhone && validVerificationCode) {
        
        [self authorizeWithPhone:cleanPhone
                      completion:^(BOOL isAuthorized) {
                          
                          [[AIRAPIManager sharedAPI]
                           confirmCode:[self.verificationCodeTextField.text integerValue]
                           phone:cleanPhone
                           callback:^(BOOL confirmation, NSError *error) {
                               
                               NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kDeviceToken];
                               
                               if (deviceToken) {
                                   [[AIRAPIManager sharedAPI] setDeviceToken:deviceToken
                                                                    callback:^(BOOL complete, NSError *error) {
                                                                        if (complete) {
                                                                            DLog(@"setDeviceToken");
                                                                        }
                                                                    }];
                               }
                               
                               if (confirmation) {
                                   [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserAuthorized];
                                   [self showEstatesControllerFromTabBar];
                               } else {
                                   self.verificationCodeTextField.text = @"";
                                   [self displayConfirmationAlert];
                               }
                           }];
                      }];
    }
}

- (IBAction)resendVerificationCodeButtonPressed:(id)sender {
    [self resendVerificationCode];
}

#pragma mark - Actions

- (void)authorizeWithPhone:(NSString *)phone
                completion:(void(^)(BOOL isAuthorized))completion {
    [[AIRAPIManager sharedAPI]
     authorizeWithPhone:phone
     password:@"12345"
     callback:^(NSString *accessToken, NSError *error) {

         [[NSUserDefaults standardUserDefaults] setObject:phone forKey:kPhoneNumber];
         
         BOOL isStored = [self saveAccessTokenToKeychain:accessToken account:phone];
         if (isStored) {
             NSString *token = [self takeAccessTokenFromKeychainAccount:phone];
             [self changeAccessToken:token];
         }
         
         if (accessToken) {
             completion(YES);
         }
     }];
}

- (void)resendVerificationCode {
    NSString *cleanPhone = [self cleanPhone:self.phoneTextField.text];
    
    [[AIRAPIManager sharedAPI]
     restoreVerificationCodeWithPhone:cleanPhone
     callback:^(NSString *verificationCode, NSError *error) {
         DLog(@"verification_code = %@", verificationCode);
     }];
}

#pragma mark - Token

- (NSString *)takeAccessTokenFromKeychainAccount:(NSString *)phone {
    NSString *token = [SAMKeychain passwordForService:@"com.hivery.token" account:phone];
    DLog(@"takeAccessTokenFromKeychain = %@",token);
    return token;
}

- (BOOL)saveAccessTokenToKeychain:(NSString *)accessToken account:(NSString *)phone {
    BOOL result = [SAMKeychain setPassword:accessToken forService:@"com.hivery.token" account:phone];
    return result;
}

- (void)changeAccessToken:(NSString *)accessToken {
    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeAccessToken object:accessToken];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([self.verificationCodeTextField isFirstResponder]) {
        self.verificationCodeTextField.text = @"";
    }
}

//Return
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSString *cleanPhone = [self cleanPhone:self.phoneTextField.text];
    BOOL validPhone = [AIRValidator validatePhone:cleanPhone];
    BOOL validVerificationCode = [AIRValidator validateVerificationCode:self.verificationCodeTextField.text];
    
    if ([self.phoneTextField isFirstResponder]) {
        if (validPhone && self.verificationCodeTextField.text.length == 0) {
            [self.verificationCodeTextField becomeFirstResponder];
            return YES;
        } else {
            [self.phoneTextField resignFirstResponder];
        }
    }

    if (validPhone && validVerificationCode) {
 
    } else {
        
        NSMutableString *alertString = [NSMutableString new];
        
        NSString *phone = [self.phoneTextField.text trimmingWhitespaces];
        NSString *verificationCode = [self.verificationCodeTextField.text trimmingWhitespaces];
        
        //text was empty or only whitespace
        if (phone.length == 0) {
            [alertString appendString:@"Phone is required"];
        } else if (!validPhone) {
            [alertString appendString:@"Invalid verification code"];
        }
        
        if (verificationCode.length == 0) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Verification code is required"];
        } else if (!validVerificationCode) {
            if(alertString.length > 0)[alertString appendString:@"\n"];
            [alertString appendString:@"Invalid password \n (contain from 5 to 15 characters)"];
        }
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:alertString message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        [alert show];
    }
    
    return YES;
}

#pragma mark - Private

- (NSString *)cleanPhone:(NSString *)phone {
    NSString *resultPhone = phone;
    resultPhone = [resultPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    resultPhone = [resultPhone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    return resultPhone;
}

- (void)displayConfirmationAlert {
    [AIRAlert alertCodeConfirmation];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == kPhoneFieldTag) {
        NSMutableString *mutablePhone = NSMutableString.new;
        [mutablePhone appendString:textField.text];
        [mutablePhone appendString:string];
        
        NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
        NSError *anError = nil;
        NBPhoneNumber *myNumber = [phoneUtil parseWithPhoneCarrierRegion:mutablePhone error:&anError];
        
        NSString *resultNumber;
        
        resultNumber = [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:&anError];
        
        if(resultNumber) {
            [textField setText:resultNumber];
        } else {
            [textField setText:mutablePhone];
        }
        
        NSLog(@"INTERNATIONAL : %@", [phoneUtil format:myNumber
                                          numberFormat:NBEPhoneNumberFormatINTERNATIONAL
                                                 error:&anError]);
        //backspace pressed
        if (range.length==1 && string.length==0) return YES;
        
        return NO;
    } else {
        
        //VERIFICATION CODE
        if ([string length] == 0 && range.length > 0) {
            return YES;
        }
        
        BOOL validNumber = [AIRValidator validateNumber:string];
        if (!validNumber) return NO;
        
        if (textField.text.length + 1 == 4) {
            NSString *resultString = StringConcat(textField.text, string);
            [textField setText:resultString];
            [textField resignFirstResponder];
            return NO;
        }
        
        if (textField.text.length + 1 == 5) {
            return NO;
        }
    }
    return YES;
}

@end

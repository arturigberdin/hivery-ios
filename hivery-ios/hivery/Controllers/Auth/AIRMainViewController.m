//
//  AIRMainViewController.m
//  hivery
//
//  Created by Artur on 30/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRMainViewController.h"

//Navigation
#import "UIViewController+Navigation.h"

//Layout
#import "AIRMainViewController+Layout.h"

@interface AIRMainViewController ()

@end

@implementation AIRMainViewController

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //[self layoutLogoView];
    //[self layoutGetStartedLabel];
    //[self layoutButtonsContainerView];
}

#pragma mark - Events

- (IBAction)setupProfilePressed:(id)sender {
    [self showQuestionsController];
}

- (IBAction)loginPressed:(id)sender {
    [self showLoginController];
}

#pragma mark - Layouts

- (void)layoutLogoView {

    [self.logoImageView makeConstraints:^(MASConstraintMaker *make) {

        make.width.equalTo(@150);
        make.height.equalTo(@150);
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY).multipliedBy(0.5);
    }];
}

- (void)layoutGetStartedLabel {
    [self.startLabel makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@157);
        make.height.equalTo(@57);
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY).multipliedBy(1.0);
    }];
}

- (void)layoutButtonsContainerView {
    [self.buttonsContainer makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@278);
        make.height.equalTo(@128);
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY).multipliedBy(1.325);
    }];
}

@end

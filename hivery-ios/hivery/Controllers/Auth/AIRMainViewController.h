//
//  AIRMainViewController.h
//  hivery
//
//  Created by Artur on 30/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AIRMainViewController : UIViewController

@property (weak, nonatomic) IBOutlet AXWireButton *setUpProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *startLabel;

@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;

@end

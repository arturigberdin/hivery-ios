//
//  DraggableViewBackground.m
//  testing swiping
//
//  Created by Richard Kim on 8/23/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//

#import "DraggableViewBackground.h"

#import <QuartzCore/QuartzCore.h>

@interface DraggableViewBackground ()

//Content
@property(strong, nonatomic) NSArray *estatesCards;

@end

@implementation DraggableViewBackground{
    NSInteger cardsLoadedIndex; //%%% the index of the card you have loaded into the loadedCards array last
//    NSMutableArray *loadedCards; //%%% the array of card loaded (change max_buffer_size to increase or decrease the number of cards this holds)
}
//this makes it so only two cards are loaded at a time to
//avoid performance and memory costs
static const int MAX_BUFFER_SIZE = 3; //%%% max number of cards loaded at any given time, must be greater than 1

//#define CARD_HEIGHT = 350; //%%% height of the draggable card
//#define CARD_WIDTH = 250; //%%% width of the draggable card

@synthesize exampleCardLabels; //%%% all the labels I'm using as example data at the moment
@synthesize allCards, loadedCards;//%%% all the cards

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [super layoutSubviews];
        
        loadedCards = [[NSMutableArray alloc] init];
        allCards = [[NSMutableArray alloc] init];
        cardsLoadedIndex = 0;
        
    }
    return self;
}

#pragma mark - Private

- (DraggableView *)createDraggableViewWithDataAtIndex:(NSInteger)index
{
    CGFloat CARD_WIDTH = ScreenWidth*0.95;
    CGFloat CARD_HEIGHT = ScreenHeight*0.85;
    
    if (index >= 2) {
        index = 2;
    }
    
    DraggableView *draggableView =[[DraggableView alloc] initWithFrame:
                                   CGRectMake((ScreenWidth - CARD_WIDTH)*0.5,
                                              (ScreenHeight - CARD_HEIGHT)*(0.275 + 0.3*index),
                                              CARD_WIDTH,
                                              CARD_HEIGHT)];
    
    CGFloat scale = (1 - index*0.05); //0.95 ... 0.80
    draggableView.transform = CGAffineTransformMakeScale(scale, scale);
    
    draggableView.layer.cornerRadius = 10;
    draggableView.layer.masksToBounds = YES;
    
    draggableView.delegate = self;
    
    if (index == 0) {
        [draggableView setY:((ScreenHeight - CARD_HEIGHT)*0.275)];
        draggableView.alpha = 1.0;
    }
    
    if (index == 1) {
        [draggableView setY:((ScreenHeight - CARD_HEIGHT)*0.675)];
        draggableView.alpha = 0.9;
    }
    
    if (index == 2) {
        [draggableView setY:((ScreenHeight - CARD_HEIGHT)*1.075)];
        draggableView.alpha = 0.8;
    }
    
    return draggableView;
}

- (void)loadCards
{
    if([self.estatesCards count] > 0) {
        NSInteger numLoadedCardsCap = (([self.estatesCards count] > MAX_BUFFER_SIZE)?MAX_BUFFER_SIZE:[self.estatesCards count]);

        for (int i = 0; i<[self.estatesCards count]; i++) {
            
            DraggableView* newCard = [self createDraggableViewWithDataAtIndex:i];
            
            newCard.layer.cornerRadius = 10;
            newCard.layer.masksToBounds = YES;
            
            [newCard setupWithEstate:self.estatesCards[i]];

            [allCards addObject:newCard];
            
            if (i<numLoadedCardsCap) {
                //%%% adds a small number of cards to be loaded
                [loadedCards addObject:newCard];
            }
        }

        for (int i = 0; i<[loadedCards count]; i++) {
            if (i>0) {
                [self insertSubview:[loadedCards objectAtIndex:i] belowSubview:[loadedCards objectAtIndex:i-1]];
            } else {
                [self addSubview:[loadedCards objectAtIndex:i]];
            }
            cardsLoadedIndex++; //%%% we loaded a card into loaded cards, so we have to increment
        }
    }
}

#pragma mark - Actions

- (void)swipeRight
{
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeRight;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView rightClickAction];
}

- (void)swipeLeft
{
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeLeft;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView leftClickAction];
}

#pragma mark - Public

- (void)cardSwipedLeft:(UIView *)card;
{
    //do whatever you want with the card that was swiped
    //    DraggableView *c = (DraggableView *)card;
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    
    [self calculateReplacementOfCards];
}

- (void)cardSwipedRight:(UIView *)card
{
    //do whatever you want with the card that was swiped
    //    DraggableView *c = (DraggableView *)card;
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    
    [self calculateReplacementOfCards];
}

- (void)calculateReplacementOfCards {
    
    int index = 0;
    for (DraggableView *card in loadedCards) {

        CGFloat CARD_HEIGHT = ScreenHeight*0.85;
        
        if (index >= 3) {
            index = 2;
        }
        
        CGFloat scale = (1 - index*0.05); //0.9 ... 0.8
        card.transform = CGAffineTransformMakeScale(scale, scale);
        
        if (index == 0) {
            [card setY:((ScreenHeight - CARD_HEIGHT)*0.275)];
            card.alpha = 1.0;
        }
        if (index == 1) {
            [card setY:((ScreenHeight - CARD_HEIGHT)*0.675)];
            card.alpha = 0.9;
        }
        if (index == 2) {
            [card setY:((ScreenHeight - CARD_HEIGHT)*1.075)];
            card.alpha = 0.8;
        }
        
        index++;
    }
}

#pragma mark - Public

- (void)setupCards:(NSArray *)cards
{
    self.estatesCards = cards;
    [self loadCards];
}

@end

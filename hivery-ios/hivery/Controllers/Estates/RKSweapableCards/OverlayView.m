//
//  OverlayView.m
//  testing swiping
//
//  Created by Richard Kim on 5/22/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests

#import "OverlayView.h"

@implementation OverlayView
@synthesize imageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setMode:(GGOverlayViewMode)mode {
    
    if(mode == GGOverlayViewModeLeft) {
        self.backgroundColor = UIColorFromRGB(0xEE4054);
    } else {
        self.backgroundColor = UIColorFromRGB(0x2ECC71);
    }
    
    if (_mode == mode) {
        return;
    }
    
    _mode = mode;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end

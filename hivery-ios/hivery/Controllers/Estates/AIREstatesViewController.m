//
//  AIREstatesViewController.m
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIREstatesViewController.h"

//Cards
#import "DraggableViewBackground.h"

//API
#import "AIRAPIManager.h"

//App Delegate
#import "AppDelegate.h"

#import <QuartzCore/QuartzCore.h>

@interface AIREstatesViewController ()

@property (strong, nonatomic) DraggableViewBackground *draggableBackground;

@property (weak, nonatomic) IBOutlet UILabel *emptyCardsLabel;
@property (strong, nonatomic) AppDelegate *appDelegate;

@property (assign, atomic) BOOL isLoaded;

@end

@implementation AIREstatesViewController

@synthesize appDelegate;

#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    appDelegate =  [[UIApplication sharedApplication] delegate];

    self.draggableBackground = [[DraggableViewBackground alloc] initWithFrame:ScreenBounds];
    [self.view addSubview:self.draggableBackground];
    
    self.draggableBackground.layer.cornerRadius = 10;
    self.draggableBackground.layer.masksToBounds = YES;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    DLog(@"viewDidAppear");
    
    self.emptyCardsLabel.hidden = YES;
    
    if (!self.draggableBackground.loadedCards.count && _isLoaded == NO) {
        [self loadCards];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    DLog(@"viewDidDisappear");
    
    self.emptyCardsLabel.hidden = YES;
}

- (void)loadCards {
    DLog(@"loadCards");
    self.isLoaded = YES;
    
    [appDelegate disableTabs];
    
    [[AIRAPIManager sharedAPI] getEstatesWithCallback:^(NSArray *estates, NSError *error) {

        [self.draggableBackground setupCards:estates];
        
        DLog(@"estates = %@", estates);
        
        self.isLoaded = NO;
        [appDelegate enableTabs];
        
        [UIView animateWithDuration:0.25f
                         animations:^{
                             
                             [self.view sendSubviewToBack:self.emptyCardsLabel];
                             self.emptyCardsLabel.hidden = NO;
                         }];
    }];
}

@end

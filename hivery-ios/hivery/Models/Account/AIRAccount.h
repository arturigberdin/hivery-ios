//
//  AIRAccount.h
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIRAccount : NSObject

@property (assign, nonatomic) NSInteger accountID;

@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *password;
@property (copy, nonatomic) NSString *role;
@property (copy, nonatomic) NSString *verification_code;

@property (copy, nonatomic) NSString *occupation;
@property (copy, nonatomic) NSString *companyName;

//Containers
@property (copy, nonatomic) NSMutableArray *bedroomsContainer;
@property (copy, nonatomic) NSMutableArray *districtsContainer;

- (instancetype)initWithResponse:(NSDictionary *)response;

@end

//
//  AIRAccount.m
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAccount.h"

@implementation AIRAccount

- (instancetype)initWithResponse:(NSDictionary *)response {
    
    self = [super init];
    if (self) {
        _accountID = [[response optionalObjectForKey:@"id"] integerValue];
        
        _name = [response optionalObjectForKey:@"name"];
        _email = [response optionalObjectForKey:@"email"];
        
        _phone = [response optionalObjectForKey:@"phone"];
        
        _role = [response optionalObjectForKey:@"role"];
        
        _occupation = [response optionalObjectForKey:@"occupation"];
        
        _companyName = [response optionalObjectForKey:@"company_name"];
        
        
        _bedroomsContainer = [NSMutableArray new];
        _districtsContainer = [NSMutableArray new];
        
        NSArray *searchValues = [response optionalObjectForKey:@"search_values"];
        for (NSDictionary *object in searchValues) {
            NSDictionary *searchParam = [object optionalObjectForKey:@"search_param"];
            
            NSString *name = [searchParam optionalObjectForKey:@"name"];
            
            if ([name isEqualToString:@"How many rooms does it have?"]) {
                
                NSDictionary *searchValue = [object optionalObjectForKey:@"search_value"];
                NSInteger bedrooms = [[searchValue optionalObjectForKey:@"value"] integerValue];
                
                [_bedroomsContainer addObject:@(bedrooms)];
            }
        }
        
        
        for (NSDictionary *object in searchValues) {
            NSDictionary *searchParam = [object optionalObjectForKey:@"search_param"];
            
            NSString *name = [searchParam optionalObjectForKey:@"name"];
            
            if ([name isEqualToString:@"Which areas would you like to live in?"]) {
                
                NSDictionary *searchValue = [object optionalObjectForKey:@"search_value"];
                NSString * district = [searchValue optionalObjectForKey:@"value"];
                
                [_districtsContainer addObject:district];
            }
        }
        
    }
    return self;
}

@end
//
//  AIRAnswer.m
//  hivery
//
//  Created by Artur on 23/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAnswer.h"

@implementation AIRAnswer

- (NSArray *)generateAnswerData {
    
    NSString *questionID =
    [NSString stringWithFormat:@"questions[%ld][id]=%ld", (long)_answerIndex, (long)_questionID];
    
    NSString *fieldName =
    [NSString stringWithFormat:@"questions[%ld][field_name]=%ld", (long)_answerIndex, (long)_fieldName];
    
    NSMutableArray *answerContainer = [NSMutableArray new];
    for (NSString *value in _answers) {
        
        NSString *answer =
        [NSString stringWithFormat:@"questions[%ld][answer][]=%@", (long)_answerIndex, value];
        
        [answerContainer addObject:answer];
    }
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:questionID];
    [paramsArray addObject:fieldName];
    
    for (NSString *object in answerContainer) {
        [paramsArray addObject:object];
    }
    
    return [paramsArray copy];
}

- (BOOL)validateAnswer {
    return YES;
}

@end

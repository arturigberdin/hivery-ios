//
//  AIRAnswer.h
//  hivery
//
//  Created by Artur on 23/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIRAnswer : NSObject

//main
@property (assign, nonatomic) NSInteger questionID;

@property (copy, nonatomic)   NSString *value;

@property (strong, nonatomic) NSArray *answers;

@property (copy, nonatomic) NSString *fieldName;
@property (assign, nonatomic) NSInteger answerIndex;

- (NSArray *)generateAnswerData;

- (BOOL)validateAnswer;

@end

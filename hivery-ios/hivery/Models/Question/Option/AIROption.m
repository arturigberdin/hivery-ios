//
//  AIROption.m
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIROption.h"

@implementation AIROption

- (instancetype)initWithResponse:(NSDictionary *)response {
    
    self = [super init];
    if (self) {
        _optionID = [[response optionalObjectForKey:@"id"] integerValue];
        _value = [response optionalObjectForKey:@"value"];
    }
    
    return self;
}

+ (NSArray *)optionsWithResponse:(id)response {

    NSMutableArray *container = [NSMutableArray new];
    NSArray *responseArray = (NSArray *)response;
    
    //Mapping response to options
    for (NSDictionary *responseObject in responseArray) {
        AIROption *option = [[AIROption alloc] initWithResponse:responseObject];
        [container insertObject:option atIndex:container.count];
    }
    
    return [container copy];
}

@end

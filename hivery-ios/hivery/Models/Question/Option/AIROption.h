//
//  AIROption.h
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIROption : NSObject

@property (assign, nonatomic) NSInteger optionID;
@property (copy, nonatomic) NSString *value;

- (instancetype)initWithResponse:(NSDictionary *)response;

+ (NSArray *)optionsWithResponse:(id)response;

@end

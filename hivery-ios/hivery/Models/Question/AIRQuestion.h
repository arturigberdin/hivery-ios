//
//  AIRQuestion.h
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIRQuestion : NSObject

@property (assign, nonatomic) NSInteger questionID;

@property (assign, nonatomic) NSInteger priority;

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *selectType;
@property (copy, nonatomic) NSString *fieldName;

//Array of AIROption objects
@property (strong, nonatomic) NSArray *options;

//Array of strings
@property (strong, nonatomic) NSArray *answers;

- (instancetype)initWithResponse:(NSDictionary *)response;

+ (NSArray *)questionsWithResponse:(id)response;

@end

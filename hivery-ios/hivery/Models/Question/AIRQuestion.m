//
//  AIRQuestion.m
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRQuestion.h"

//Models
#import "AIROption.h"

@implementation AIRQuestion

- (instancetype)initWithResponse:(NSDictionary *)response {
    
    self = [super init];
    if (self) {
        _questionID = [[response optionalObjectForKey:@"id"] integerValue];
        _name = [response optionalObjectForKey:@"name"];
        _priority = [[response optionalObjectForKey:@"priority"] integerValue];
        _selectType = [response optionalObjectForKey:@"select_type"];
        _fieldName = [response optionalObjectForKey:@"field_name"];
    }
    return self;
}

+ (NSArray *)questionsWithResponse:(id)response {
    
    NSMutableArray *container = [NSMutableArray new];
    NSArray *responseArray = (NSArray *)response;
    
    //Mapping response to questions
    for (NSDictionary *responseObject in responseArray) {
        
        AIRQuestion *question = [[AIRQuestion alloc] initWithResponse:responseObject];
        
        NSArray *optionsResponse = [responseObject optionalObjectForKey:@"options"];
        
        NSArray *options = [AIROption optionsWithResponse:optionsResponse];
        question.options = options;
        
        //Add question to container
        [container insertObject:question atIndex:container.count];
    }
    
    return [container copy];
}

@end

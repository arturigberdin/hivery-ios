//
//  AIRAccessToken.h
//  hivery
//
//  Created by Artur on 18/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIRAccessToken : NSObject

@property (copy, nonatomic) NSString *token;

@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *email;
@property (assign, nonatomic) NSInteger password;

@end

//
//  AIREstate.m
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIREstate.h"

@implementation AIREstate

- (instancetype)initWithResponse:(NSDictionary *)response {
    
    self = [super init];
    if (self) {
        _estateID = [[response optionalObjectForKey:@"id"] integerValue];
        _name = [response optionalObjectForKey:@"name"];
        
        _images = [NSMutableArray new];
        NSArray *images = [response optionalObjectForKey:@"images"];
        
        for (NSDictionary *object in images) {
            
            NSString *url = [object optionalObjectForKey:@"url"];
            [_images addObject:url];
        }
        
        NSArray *properties = [response optionalObjectForKey:@"properties"];
        
        for (NSDictionary *object in properties) {
            NSDictionary *property = [object optionalObjectForKey:@"property"];
            NSString *name = [property optionalObjectForKey:@"name"];
            
            if ([name isEqualToString:@"Address"]) {
                NSDictionary *value = [object optionalObjectForKey:@"value"];
                NSString *address = [value optionalObjectForKey:@"value"];
                _address = address;
            }
            
            if ([name isEqualToString:@"District"]) {
                NSDictionary *value = [object optionalObjectForKey:@"value"];
                NSString *district = [value optionalObjectForKey:@"value"];
                _district = district;
            }
            
            if ([name isEqualToString:@"Closing price"]) {
                NSDictionary *value = [object optionalObjectForKey:@"value"];
                NSString *price = [value optionalObjectForKey:@"value"];
                price = [price stringByReplacingOccurrencesOfString:@"S$ " withString:@""];
                price = [price stringByReplacingOccurrencesOfString:@" / month" withString:@""];
                _closingPrice = price;
            }
            
            if ([name isEqualToString:@"Number of Bedrooms"]) {
                NSDictionary *value = [object optionalObjectForKey:@"value"];
                NSString *bedrooms = [value optionalObjectForKey:@"value"];
                _bedsCount = [bedrooms integerValue];
            }
            
            if ([name isEqualToString:@"Number of Bathrooms"]) {
                NSDictionary *value = [object optionalObjectForKey:@"value"];
                NSString *bathrooms = [value optionalObjectForKey:@"value"];
                _bathsCount = [bathrooms integerValue];
            }
        }
    }
    return self;
}

+ (NSArray *)estatesWithResponse:(id)response {
    
    NSMutableArray *container = [NSMutableArray new];
    NSArray *responseArray = (NSArray *)response;
    
    //Mapping response to array of estates
    for (NSDictionary *responseObject in responseArray) {
        AIREstate *estate = [[AIREstate alloc] initWithResponse:responseObject];
        [container insertObject:estate atIndex:container.count];
    }
    
    return [container copy];
}

@end

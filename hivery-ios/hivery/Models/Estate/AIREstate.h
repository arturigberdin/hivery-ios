//
//  AIREstate.h
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIREstate : NSObject

/*
{
    "id": 58,
    "name": "Chancery Grove",
    "tags": null,
    "questions": null,
    "images": [],
    "created_at": "2016-07-29T15:16:01.968Z",
    "updated_at": "2016-07-29T15:16:03.396Z"
 
    properties=({
        property={
            id=78;  
            name="Number of Bedrooms";
        };
        value={
            id=2485;
            value="2";
        };
    },
    )
},
*/

@property (assign, nonatomic) NSInteger estateID;
@property (copy, nonatomic) NSString *name;
@property (strong, nonatomic) NSURL *imageUrl;

@property (strong, nonatomic) NSMutableArray *images;

@property (copy, nonatomic) NSString *address;
@property (copy, nonatomic) NSString *district;

@property (assign, nonatomic) NSInteger bedsCount;
@property (assign, nonatomic) NSInteger bathsCount;

@property (copy, nonatomic) NSString *closingPrice;


- (instancetype)initWithResponse:(NSDictionary *)response;

+ (NSArray *)estatesWithResponse:(id)response;

@end

//
//  AppDelegate.m
//  hivery
//
//  Created by Artur on 15/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AppDelegate.h"

//Fonts
#import <CoreText/CoreText.h>
#import <MobileCoreServices/MobileCoreServices.h>

//Bug report
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

//Logger
#import "AFNetworkActivityLogger/AFNetworkActivityLogger.h"

//Controllers
#import "AIRProfileViewController.h"
#import "AIREstatesViewController.h"
#import "AIRScheduleViewController.h"
#import "AIRMainViewController.h"

//Base Controllers
#import "AIRBaseTabBarController.h"
#import "AIRProfileNavigationController.h"

@interface AppDelegate ()

@property (strong, nonatomic)AIRBaseTabBarController *tabVC;

@end

@implementation AppDelegate

@synthesize tabVC;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [application registerUserNotificationSettings:mySettings];
    }
    
    [self p_loadCustomFonts];
    [self p_setupNetworkLogger];
    
    [self p_setupFabric];
    
    BOOL userAuthorized = [[NSUserDefaults standardUserDefaults] boolForKey:kUserAuthorized];
    
    if (userAuthorized) {
        [self showEstatesCardsController];
    } else {
        [self showMainController];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    //Nulling badge when open app
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.givolabs.hivery" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"hivery" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"hivery.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Private

- (void)p_loadCustomFonts {
    
    // Load custom fonts
    CTFontManagerRegisterFontsForURLs((__bridge CFArrayRef)((^{
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSURL *resourceURL = [[NSBundle mainBundle] resourceURL];
        NSArray *resourceURLs = [fileManager contentsOfDirectoryAtURL:resourceURL includingPropertiesForKeys:nil options:0 error:nil];
        return [resourceURLs filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSURL *url, NSDictionary *bindings) {
            CFStringRef pathExtension = (__bridge CFStringRef)[url pathExtension];
            NSArray *allIdentifiers = (__bridge_transfer NSArray *)UTTypeCreateAllIdentifiersForTag(kUTTagClassFilenameExtension, pathExtension, CFSTR("public.font"));
            if (![allIdentifiers count]) {
                return NO;
            }
            CFStringRef utType = (__bridge CFStringRef)[allIdentifiers lastObject];
            return (!CFStringHasPrefix(utType, CFSTR("dyn.")) && UTTypeConformsTo(utType, CFSTR("public.font")));
            
        }]];
    })()), kCTFontManagerScopeProcess, nil);
    
    
     #ifdef DEBUG
     //Check loaded fonts (search by names in console)
     for (NSString* family in [UIFont familyNames]) {
     DLog(@"%@", family);
     for (NSString* name in [UIFont fontNamesForFamilyName: family]) {
     DLog(@"  %@", name);
     }
     }
     #endif
}

- (void)p_setupFabric {
    [Fabric with:@[[Crashlytics class]]];
}

- (void)p_setupNetworkLogger {
#ifdef DEBUG
    [[AFNetworkActivityLogger sharedLogger] startLogging];
#endif
}

#pragma mark - Navigation

- (void)showEstatesCardsController
{
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    
    AIRProfileViewController *vc1 = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRProfileViewController.class)];
    AIREstatesViewController *vc2 = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIREstatesViewController.class)];
    AIRScheduleViewController *vc3 = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRScheduleViewController.class)];
    
    AIRProfileNavigationController *navVC1 = [[AIRProfileNavigationController alloc] initWithRootViewController:vc1];
    
    AIRProfileNavigationController *navVC3 = [[AIRProfileNavigationController alloc] initWithRootViewController:vc3];
    
    tabVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRBaseTabBarController.class)];
    tabVC.viewControllers = [NSArray arrayWithObjects:navVC1, vc2, navVC3, nil];
    
    [self.window setRootViewController:tabVC];
    //Show second tab in tabbar
    [tabVC setSelectedIndex:1];
}

- (void)showMainController {
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    
    AIRMainViewController *mainVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRMainViewController.class)];
    
    [self.window setRootViewController:nil];
    [self.window setRootViewController:mainVC];
}

#pragma mark - Tabs

- (void)disableTabs {
    [[[[tabVC tabBar] items]objectAtIndex:0] setEnabled:NO];
    [[[[tabVC tabBar] items]objectAtIndex:2] setEnabled:NO];
}

- (void)enableTabs {
    [[[[tabVC tabBar] items]objectAtIndex:0] setEnabled:YES];
    [[[[tabVC tabBar] items]objectAtIndex:2] setEnabled:YES];
}

#pragma mark - Remote Notifications

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        NSLog(@"didRegisterUser");
        [application registerForRemoteNotifications];
    }
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    DLog(@"device_token = %@", token);
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:kDeviceToken];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSLog(@"Error in registration. Error: %@", err);
}

@end

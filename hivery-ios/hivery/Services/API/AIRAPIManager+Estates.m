//
//  AIRAPIManager+Estates.m
//  hivery
//
//  Created by Artur on 17/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAPIManager.h"

//Models
#import "AIREstate.h"

@interface AIRAPIManager ()

@property(nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation AIRAPIManager (Estates)

/*
- (void)postEstatesChoice {

    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    NSDictionary *params = @{@"reaction" : @"like"
                             };

    NSInteger estateNum = 52;
    NSString *kChoiceEstate = [NSString stringWithFormat:@"estates/%ld/claim", (long)estateNum];
    
    [self.sessionManager GET:StringConcat(kAPIVersion, kChoiceEstate)
                  parameters:params
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         DLog(@"responseObject = %@", responseObject);
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         
                     }];
}
*/

/*
- (void)postEstatesChoice {
    
    NSInteger estateNum = 52;
    NSString *kChoiceEstate = [NSString stringWithFormat:@"estates/%ld/claim", (long)estateNum];
    
    NSString *body = @"reaction=dislike";
    
    [self.sessionManager
    POST:StringConcat(kAPIVersion, kChoiceEstate)
    parameters:nil
    constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *data = [body dataUsingEncoding:NSUTF8StringEncoding];
        [formData appendPartWithHeaders:nil body:data];
        //[formData appendPartWithFormData:[NSData dataWithBytes:[body UTF8String]] name:@"Text"];
    }
    progress:nil
    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    }
    failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}
*/

//estates.get
- (void)getEstatesWithCallback:(void (^)(NSArray *estates, NSError *))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));

    [self.sessionManager GET:kGetEstates
                  parameters:nil
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         DLog(@"responseObject = %@", responseObject);
                         NSArray *array = (NSArray *)responseObject;
                         NSArray *estates = [AIREstate estatesWithResponse:array];
                         
                         callback(estates, nil);
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         
                         if (error.code == kCFURLErrorNotConnectedToInternet) [AIRAlert alertNoInternetConnection];
                         
                         callback(nil, error);
                     }];
}

//estates.schedule
- (void)scheduleEstatesWithCallback:(void (^)(NSArray *estates, NSError *))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    [self.sessionManager GET:kScheduleEstates
                  parameters:nil
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         DLog(@"responseObject = %@", responseObject);
                         NSArray *array = (NSArray *)responseObject;
                         NSArray *estates = [AIREstate estatesWithResponse:array];
                         
                         callback(estates, nil);
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         
                         if (error.code == kCFURLErrorNotConnectedToInternet) [AIRAlert alertNoInternetConnection];
                         
                         callback(nil, error);
                     }];
}

- (void)postEstateChoice:(NSInteger)estateID
                reaction:(NSString *)reaction
                callback:(void (^)(BOOL complete, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    NSString *kChoiceEstate = [NSString stringWithFormat:@"estates/%ld/claim", (long)estateID];
    NSString *urlString = StringConcat(kBaseURLString, kChoiceEstate);
    NSURL *urlRequest = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlRequest];
    request.HTTPMethod = @"POST";
    
    NSString *bodyRequest = [NSString stringWithFormat:@"reaction=%@", reaction];
    request.HTTPBody = [bodyRequest dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *token = self.accessToken.token;
    NSString *authorizationToken = [NSString stringWithFormat:@"Bearer %@", token];
    [request setValue:authorizationToken forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionConfiguration* configureSession = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession* session = [NSURLSession sessionWithConfiguration:configureSession];
    
    NSURLSessionDataTask *dataTask =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                   
                   //NSHTTPURLResponse *responseObject = (NSHTTPURLResponse *)response;
                   if (!error) {
                       NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                       
                       DLog(@"responseDict = %@", responseDict);
                       if (responseDict) {
                           callback(YES, nil);
                       }
                   } else {
                       callback(NO, error);
                   }
               }];
    
    [dataTask resume];
}

//estate.choice
- (void)choiceEstate:(NSInteger)estateID
            reaction:(NSString *)reaction
            callback:(void (^)(BOOL complete, NSError *error))callback {
    
    NSDictionary *params = @{@"id"       : @(estateID),
                             @"reaction" : reaction
                             };
    
    [self.sessionManager POST:kChoiceEstate
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          callback(YES, nil);
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        
                          callback(NO, error);
                     }];
}

//estate.make_bid
- (void)makeBidEstate:(NSInteger)estateID
             callback:(void (^)(BOOL complete, NSError *error))callback {
    
    NSDictionary *params = @{@"id"       : @(estateID)
                             };
    
    [self.sessionManager POST:kMakeBidEstate
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          callback(YES, nil);
                     }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          callback(NO, error);
                     }];
}

@end

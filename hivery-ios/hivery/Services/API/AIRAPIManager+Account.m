//
//  AIRAPIManager+Account.m
//  hivery
//
//  Created by Artur on 17/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAPIManager.h"

//Models
#import "AIRAccount.h"
#import "AIRAnswer.h"

@interface AIRAPIManager ()

@property(nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation AIRAPIManager (Account)

/** account.register
    
    @logic
    Call before authorization, register and save user to database only one-time
 
    @return 
    verification_code
*/
- (void)registerWithUsername:(NSString *)name
                       email:(NSString *)email
                       phone:(NSString *)phone
                    password:(NSString *)password
                    callback:(void(^)(NSString *verificationCode, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    NSDictionary *params = @{@"username" : name,
                             @"email"    : email,
                             @"phone"    : phone,
                             @"password" : password
                             };
    
    [self.sessionManager POST:kRegister
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          DLog(@"responseObject = %@", responseObject);
                          NSDictionary *response = (NSDictionary *)responseObject;
                          NSString *verificationCode = [response optionalObjectForKey:@"verification_code"];
                          
                          callback(verificationCode, nil);
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          
                          callback(nil, error);
                      }];
}

//account.confirmCode
- (void)confirmCode:(NSInteger)verification_code
              phone:(NSString *)phone
           callback:(void(^)(BOOL confirmation, NSError *error))callback {
    
    NSDictionary *params = @{@"phone"                : phone,
                             @"verification_code"    : @(verification_code)
                             };
    
    [self.sessionManager POST:kConfirmVerificationCode
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          NSNumber *isConfirmed = (NSNumber *)[responseObject optionalObjectForKey:@"confirmed"];
                          callback([isConfirmed boolValue], nil);
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          
                          if (error.code == kCFURLErrorNotConnectedToInternet) [AIRAlert alertNoInternetConnection];
                          
                          callback(NO, error);
                      }];
}

//account.getSearchParams
- (void)getSearchParamsWithCallback:(void(^)(NSArray *searchParams, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    [self.sessionManager GET:kGetSearchParams
                  parameters:nil
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         DLog(@"responseObject = %@", responseObject);
                         
//                         NSArray *questions = [AIRQuestion questionsWithResponse:responseObject];
//                         callback(questions, nil);
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         callback(nil, error);
                     }];
}

//account.t_saveSearchParams (testable)

- (void)saveWithSearchParamsWithAnswers:(NSArray *)answers //id,value
                               callback:(void (^)(AIRAccount *account, NSError *error))callback {
    
    NSMutableArray *objectContainer = [NSMutableArray new];
    for (AIRAnswer *answer in answers) {
        
        NSDictionary *object = @{@"id"           : @(answer.questionID),
                                 @"search_value" : answer.answers};
        
        [objectContainer addObjectsFromArray:@[object]];
    }

    NSDictionary *paramsJson = @{ @"search_values" : [objectContainer copy]};
    
    [self.sessionManager POST:kSaveSearchParams
                   parameters:paramsJson
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          DLog(@"responseObject = %@", responseObject);
                          AIRAccount *account = [[AIRAccount alloc] initWithResponse:responseObject];
                          
                          callback(account, nil);
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          callback(nil, error);
                      }];
    
}

- (void)t_saveSearchParamsWithAnswers:(NSString *)answers
                             callback:(void (^)(AIRAccount *account, NSError *error))callback {
    
//    --form 'search_values[][id]=17' \
//    --form 'search_values[][search_value][]=1' \
//    --form 'search_values[][id]=30' \
//    --form 'search_values[][search_value][]=District 1 - Raffles Place & Marina Bay' \
//    --form 'search_values[][search_value][]=District 2 - Tanjong Pagar & Chinatown'
    
//    NSArray *paramsArray = @[@"search_values[][id]=17",
//                             @"search_values[][search_value][]=1",
//                             @"search_values[][id]=30",
//                             @"search_values[][search_value][]=District 1 - Raffles Place & Marina Bay",
//                             @"search_values[][search_value][]=District 2 - Tanjong Pagar & Chinatown"
//                             ];
    
    NSDictionary *paramsJson = @{ @"search_values" :
                                  @[@{@"id": @(17), @"search_value":@[@"1"]},
                                    @{@"id": @(30), @"search_value":@[@"District 1 - Raffles Place & Marina Bay"]}
                                   ]
                             };
    
//    NSDictionary *params = @{
//                             @"search_values[0][id]"               : @"6",
//                             @"search_values[0][search_value][]"   : @"1",
//                             @"search_values[1][id]"               : @"7",
//                             @"search_values[1][search_value][]"   : @"District 1 -- Raffles Place & Marina Bay",
//                             @"search_values[1][search_value][]"   : @"District 6 -- Clarke Quay & City Hall"
//                             };
    
    [self.sessionManager POST:kSaveSearchParams
                   parameters:paramsJson
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          DLog(@"responseObject = %@", responseObject);
                          AIRAccount *account = [[AIRAccount alloc] initWithResponse:responseObject];
                          
                          callback(account, nil);
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          callback(nil, error);
                      }];
}

//account.getInfo
- (void)getInfoWithCallback:(void (^)(AIRAccount *account, NSError *error))callback {
    
    [self.sessionManager GET:kGetInfo
                  parameters:nil
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         DLog(@"responseObject = %@", responseObject);
                         
                         AIRAccount *account = [[AIRAccount alloc] initWithResponse:responseObject];
                         
                         callback(account, nil);
                         
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         
                         if (error.code == kCFURLErrorNotConnectedToInternet) [AIRAlert alertNoInternetConnection];
                         
                         callback(nil, error);
                     }];
}

//account.setInfo

- (void)setInfoWithUsername:(NSString *)username
                      email:(NSString *)email
                 occupation:(NSString *)occupation
                companyName:(NSString *)companyName
                   callback:(void (^)(AIRAccount *account, NSError *error))callback {
    
    NSDictionary *params = @{@"email"         : email,
                             @"username"      : username,
                             @"occupation"    : occupation,
                             @"company_name"  : companyName,
                             @"poll_completed": @YES
                             };
    
    [self.sessionManager PUT:kSetInfo
                  parameters:params
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         DLog(@"responseObject = %@", responseObject);
                         AIRAccount *account = [[AIRAccount alloc] initWithResponse:responseObject];
                         
                         callback(account, nil);
                         
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         callback(nil, error);
                     }];
    
    
}

- (void)setInfoWithCallback:(void (^)(AIRAccount *account, NSError *error))callback {
    
    NSDictionary *params = @{@"username"      : @"Arthur7",
                             @"occupation"    : @"Developer",
                             @"company_name"  : @"Givola",
                             @"poll_completed": @YES
                             };
 
    
    [self.sessionManager PUT:kSetInfo
                  parameters:params
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         DLog(@"responseObject = %@", responseObject);
                         AIRAccount *account = [[AIRAccount alloc] initWithResponse:responseObject];
                         
                         callback(account, nil);
                         
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         
                         if (error.code == kCFURLErrorNotConnectedToInternet) [AIRAlert alertNoInternetConnection];
                         
                         callback(nil, error);
                     }];
}

- (void)setDeviceToken:(NSString *)deviceToken callback:(void (^)(BOOL, NSError *))callback {
    
    NSDictionary *params = @{@"device_token"      : deviceToken
                             };
    
    [self.sessionManager POST:kSetDeviceToken
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          DLog(@"responseObject = %@", responseObject);
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                          DLog(@"error = %@", error);
                      }];
}

- (void)removeDeviceTokenWithCallback:(void (^)(BOOL, NSError *))callback {
    
    [self.sessionManager DELETE:kRemoveDeviceToken
                     parameters:nil
                        success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            
                        }];
}

/*
- (void)postProfileInfoWithPhone:(NSString *)phone
                       questions:(NSArray *)questions
                        callback:(void (^)(AIRAccount *account, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    NSString *phoneParam = [NSString stringWithFormat:@"phone=%@",phone];
    
    NSMutableArray *params = [NSMutableArray new];
    
    [params addObjectsFromArray:questions];
    [params insertObject:phoneParam atIndex:0];

    
    NSMutableString *paramsString = [NSMutableString new];
    for (NSString *param in params) {
        
        NSString *str = [NSString stringWithFormat:@"%@&", param];
        
        [paramsString appendString:str];
    }
    
    NSString *newString = [paramsString substringToIndex:[paramsString length]-1];
    
    //NSString *encodeParams = [newString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSData *utf8Data = [newString dataUsingEncoding:NSUTF8StringEncoding];
    

    NSDictionary *params = @{@"phone"                    : phone,
                             @"questions[0][id]"         : @"17",
                             @"questions[0][field_name]" : @"",
                             @"questions[0][answer][]"   : @"1",
                             @"questions[1][id]"         : @"30",
                             @"questions[1][field_name]" : @"",
                             @"questions[1][answer][]"   : @"Yes, furnished.",
                             @"questions[2][id]"         : @"28",
                             @"questions[2][field_name]" : @"",
                             @"questions[2][answer][]"   : @"500",
                             @"questions[3][id]"         : @"25",
                             @"questions[3][field_name]" : @"",
                             @"questions[3][answer][]"   : @"ASAP",
                             @"questions[4][id]"         : @"37",
                             @"questions[4][field_name]" : @"",
                             @"questions[4][answer][]"   : @"District 1 -- Raffles Place & Marina Bay",
                             @"questions[4][answer][]"   : @"District 6 -- Clarke Quay & City Hall",
                             @"questions[5][id]"         : @"32",
                             @"questions[5][field_name]" : @"",
                             @"questions[5][answer][]"   : @"Art",
                             @"questions[6][id]"         : @"26",
                             @"questions[6][field_name]" : @"art@email",
                             @"questions[6][answer][]"   : @"U",
                             };
 
    
    [self.sessionManager POST:StringConcat(kAPIVersion, kSaveAccountProfile)
                   parameters:utf8Data
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                          DLog(@"responseObject = %@", responseObject);
                          AIRAccount *account = [[AIRAccount alloc] initWithResponse:responseObject];
        
                          callback(account, nil);
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         callback(nil, error);
                      }];
}
*/

/*
  DEPRECATED REQUEST!
- (void)postProfileInfoWithPhone:(NSString *)phone
                       questions:(NSArray *)questions
                        callback:(void (^)(AIRAccount *account, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    NSString *urlString = StringConcat(kBaseURLString, kSaveUserProfile);
    
    NSURL *urlRequest = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlRequest];
    request.HTTPMethod = @"POST";
    
    ///// ANSWERS
    NSString *phoneValue = [NSString stringWithFormat:@"phone=%@",phone];
    phoneValue = [phoneValue stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSMutableArray *params = [NSMutableArray new];
    [params addObjectsFromArray:questions];
    [params insertObject:phoneValue atIndex:0];
    
    NSMutableString *paramsString = [NSMutableString new];
    for (NSString *param in params) {
        NSString *str = [NSString stringWithFormat:@"%@&", param];
        [paramsString appendString:str];
    }
    
    //Answers
    NSString *bodyRequest = [paramsString substringToIndex:[paramsString length]-1];
    request.HTTPBody = [bodyRequest dataUsingEncoding:NSUTF8StringEncoding];
    /////////////
    
    NSString *token = self.accessToken.token;
    NSString *authorizationToken = [NSString stringWithFormat:@"Bearer %@", token];
    [request setValue:authorizationToken forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionConfiguration* configureSession = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession* session = [NSURLSession sessionWithConfiguration:configureSession];
    
    NSURLSessionDataTask *dataTask =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                   
                   //NSHTTPURLResponse *responseObject = (NSHTTPURLResponse *)response;
                   if (!error) {
                       NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                       
                       DLog(@"responseDict = %@", responseDict);
                       if (responseDict) {
                           
                           DLog(@"responseObject = %@", responseDict);
                           AIRAccount *account = [[AIRAccount alloc] initWithResponse:responseDict];
                           
                           callback(account, nil);
                       }
                   } else {
                       callback(nil, error);
                   }
               }];
    
    [dataTask resume];
}
*/

@end

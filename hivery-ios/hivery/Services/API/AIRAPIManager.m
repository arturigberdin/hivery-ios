//
//  AIRAPIManager.m
//  hivery
//
//  Created by Artur on 17/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAPIManager.h"

//Security
#import <SAMKeychain/SAMKeychain.h>

@interface AIRAPIManager ()

@property(nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation AIRAPIManager

#pragma mark - Initialize

+ (id)sharedAPI {
    static AIRAPIManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AIRAPIManager alloc] initInstance];
    });
    
    return manager;
}

- (instancetype)initInstance {
    
    _accessToken = [[AIRAccessToken alloc] init];
    
    NSURL *baseURL = [NSURL URLWithString:kBaseURLString];
    _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL: baseURL];
    
    _sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setBearerAuthorization:)
                                                 name:kChangeAccessToken
                                               object:nil];
    
    [self setBearerAuthorization:nil];
    
    return [super init];
}

- (void)setBearerAuthorization:(NSNotification *)notification
{
    NSString *token;
    
    if (notification.object) {
        NSString *accessToken = notification.object;
        token = accessToken;
    } else {
        NSString *phone = [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneNumber];
        token = [SAMKeychain passwordForService:@"com.hivery.token" account:phone];
    }
    
    _accessToken.token = token;
    
    if ([token length] > 5) {
        [self.sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@", @"Bearer", token]
                              forHTTPHeaderField:@"Authorization"];
    } else {
        [self.sessionManager.requestSerializer setValue:@"" forHTTPHeaderField:@"Authorization"];
    }
}

- (void)logout {
    self.accessToken.token = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeAccessToken object:nil];
}


@end

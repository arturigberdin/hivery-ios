//
//  AIRNetworkConfig.h
//  hivery
//
//  Created by Artur on 17/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

//Base URL
static NSString *const kBaseURLString = @"http://rentmama-backend.herokuapp.com/api";


//Auth
static NSString *const kAuthorize               = @"auth.token"; //phone or email
static NSString *const kRestoreVerificationCode = @"auth.restoreVerificationCode";

//Account
static NSString *const kRegister                = @"account.register";
static NSString *const kConfirmVerificationCode = @"account.confirmVerificationCode";
static NSString *const kGetSearchParams         = @"account.getSearchParams";
static NSString *const kSaveSearchParams        = @"account.saveSearchParams";
static NSString *const kGetInfo                 = @"account.getInfo";
static NSString *const kSetInfo                 = @"account.setInfo";

static NSString *const kSetDeviceToken          = @"account.setDeviceToken";
static NSString *const kRemoveDeviceToken       = @"account.removeDeviceToken";

//Estates
static NSString *const kGetEstates      = @"estates.get";
static NSString *const kScheduleEstates = @"estates.schedule";
static NSString *const kChoiceEstate    = @"estate.choice";
static NSString *const kMakeBidEstate       = @"estate.make_bid";
static NSString *const kRejectEstate    = @"estate.reject";



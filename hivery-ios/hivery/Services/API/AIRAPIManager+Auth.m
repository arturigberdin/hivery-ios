//
//  AIRAPIManager+Auth.m
//  hivery
//
//  Created by Artur on 19/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAPIManager.h"

@interface AIRAPIManager ()

@property(nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation AIRAPIManager (Auth)

//auth.token (email)
- (void)authorizeWithEmail:(NSString *)email password:(NSString *)password callback:(void(^)(NSString *accessToken, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    NSDictionary *params = @{@"grant_type" : @"password",
                             @"username"   : email,
                             @"password"   : password
                             };
    
    [self.sessionManager POST:kAuthorize
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          DLog(@"responseObject = %@", responseObject);
                          NSDictionary *response = (NSDictionary *)responseObject;
                          NSString *accessToken = [response optionalObjectForKey:@"access_token"];
                          
                          callback(accessToken, nil);
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          
                          callback(nil, error);
                      }];
}

//auth.token (phone)
- (void)authorizeWithPhone:(NSString *)phone password:(NSString *)password callback:(void(^)(NSString *accessToken, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    //grant_type = assertion/password
    NSDictionary *params = @{@"grant_type" : @"password",
                             @"phone"      : phone,
                             @"password"   : @"12345"
                             };
    
    [self.sessionManager POST:kAuthorize
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                      
                          DLog(@"responseObject = %@", responseObject);
                          NSDictionary *response = (NSDictionary *)responseObject;
                          NSString *accessToken = [response optionalObjectForKey:@"access_token"];
                          
                          callback(accessToken, nil);
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          
                          if (error.code == kCFURLErrorNotConnectedToInternet) [AIRAlert alertNoInternetConnection];
                          
                          callback(nil, error);
                      }];
}

//auth.restorePasscode
- (void)restoreVerificationCodeWithPhone:(NSString *)phone
                               callback:(void (^)(NSString *code, NSError *error))callback {
    DLog(@"method = %@", NSStringFromSelector(_cmd));
    
    NSDictionary *params = @{@"phone" : phone
                             };
    
    [self.sessionManager POST:kRestoreVerificationCode
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          NSDictionary *user = [responseObject optionalObjectForKey:@"user"];
                          
                          NSString *verificationCode = [user optionalObjectForKey:@"verification_code"];
                          
                          callback(verificationCode, nil);
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          
                          if (error.code == kCFURLErrorNotConnectedToInternet) [AIRAlert alertNoInternetConnection];
                          
                          callback(nil, error);
                      }];
}

@end

//
//  AIRAPIManager.h
//  hivery
//
//  Created by Artur on 17/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

//Network
#import <AFNetworking/AFNetworking.h>

//Config
#import "AIRNetworkConfig.h"

//Models
#import "AIRAccessToken.h"

@class AIRAccount;
@class AIREstate;

@interface AIRAPIManager : NSObject

@property (nonatomic, strong) AIRAccessToken *accessToken;

+ (id)sharedAPI;

@end

@interface AIRAPIManager (Auth)

//auth.token (email)
- (void)authorizeWithEmail:(NSString *)email
                  password:(NSString *)password
                  callback:(void(^)(NSString *accessToken, NSError *error))callback;

//auth.token (phone)
- (void)authorizeWithPhone:(NSString *)phone
                  password:(NSString *)password
                  callback:(void(^)(NSString *accessToken, NSError *error))callback;

//auth.restorePasscode
- (void)restoreVerificationCodeWithPhone:(NSString *)phone callback:(void(^)(NSString *verificationCode, NSError *error))callback;

@end

@interface AIRAPIManager (Account)

//account.register
- (void)registerWithUsername:(NSString *)name
                       email:(NSString *)email
                       phone:(NSString *)phone
                    password:(NSString *)password
                    callback:(void(^)(NSString *verificationCode, NSError *error))callback;

//account.confirmCode
- (void)confirmCode:(NSInteger)verification_code
              phone:(NSString *)phone
           callback:(void(^)(BOOL confirmation, NSError *error))callback;

//account.getSearchParams
- (void)getSearchParamsWithCallback:(void(^)(NSArray *searchParams, NSError *error))callback;

//account.saveSearchParams
- (void)saveWithSearchParamsWithAnswers:(NSArray *)answers //id,value
                               callback:(void (^)(AIRAccount *account, NSError *error))callback;

//account.getInfo
- (void)getInfoWithCallback:(void (^)(AIRAccount *account, NSError *error))callback;

//account.setInfo
- (void)setInfoWithCallback:(void (^)(AIRAccount *account, NSError *error))callback;

//account.setInfo
- (void)setInfoWithUsername:(NSString *)username
                      email:(NSString *)email
                 occupation:(NSString *)occupation
                companyName:(NSString *)companyName
                   callback:(void (^)(AIRAccount *account, NSError *error))callback;

//account.setDeviceToken
- (void)setDeviceToken:(NSString *)deviceToken callback:(void (^)(BOOL complete, NSError *error))callback;

//account.removeDeviceToken
- (void)removeDeviceTokenWithCallback:(void (^)(BOOL complete, NSError *error))callback;

@end

@interface AIRAPIManager (Estates)

//estates.get
- (void)getEstatesWithCallback:(void(^)(NSArray *estates, NSError *error))callback;

//estates.schedule
- (void)scheduleEstatesWithCallback:(void (^)(NSArray *estates, NSError *))callback;

//estate.choice
- (void)choiceEstate:(NSInteger)estateID
            reaction:(NSString *)reaction
            callback:(void (^)(BOOL complete, NSError *error))callback;

//estate.make_bid
//- (void)makeBidEstate:(NSInteger)estateID
//             callback:(void (^)(BOOL complete, NSError *error))callback;

@end





//
//  AXWireButton.m
//  Pods
//

#import "AXWireButton.h"

@implementation AXWireButton

- (id)initWithCoder:(NSCoder *)aDecoder
{
  if (self = [super initWithCoder:aDecoder]) {
    [self configureAXWireButton];
  }
  return self;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    [self configureAXWireButton];
  }
  return self;
}

- (void)configureAXWireButton
{
  _borderWidth = 1.0;
  _highlightStyle = AXWireButtonHighlightStyleSimple;
}

- (void)layoutSubviews
{
  [super layoutSubviews];
  self.layer.borderWidth = _borderWidth;
  self.layer.borderColor = [[self titleColorForState:UIControlStateNormal] CGColor];
  //self.layer.cornerRadius = MIN(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds)) / 2;
  self.layer.cornerRadius = 4;
    
    
    
  [self updateDisplayWithTint:NO];
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
  if (_borderWidth != borderWidth) {
    _borderWidth = borderWidth;
    self.layer.borderWidth = borderWidth;
  }
}

- (void)setHighlighted:(BOOL)highlighted
{
  [self updateDisplayWithTint:highlighted];
}

//- (void)setSelected:(BOOL)selected
//{
//  [self updateDisplayWithTint:selected];
//}

- (void)setEnabled:(BOOL)enabled
{
  [super setEnabled:enabled];
  [self updateDisplayWithTint:NO];
}

- (void)setEmphasized:(BOOL)emphasized
{
  _emphasized = emphasized;
  [self updateDisplayWithTint:NO];
}

- (void)updateDisplayWithTint:(BOOL)isTint
{
    __block UIColor *textColor = [self titleColorForState:self.state];
  
    if (self.isEmphasized) {
    self.titleLabel.textColor = [UIColor whiteColor];
    self.backgroundColor = textColor;
    return;
  }
    
    if (_highlightStyle == AXWireButtonHighlightStyleSimple) {
        
        if (isTint) {
            textColor = self.titleLabel.tintColor;
        }
        self.backgroundColor = [UIColor clearColor];
        [UIView animateWithDuration:0.5 animations:^{
            if (isTint) {
                self.titleLabel.textColor = textColor;
                self.layer.borderColor = [textColor CGColor];
                self.layer.shadowColor = [textColor CGColor];
            } else {
                self.titleLabel.textColor = textColor;
                self.layer.borderColor = [textColor CGColor];
                self.layer.shadowColor = [textColor CGColor];
            }
        }];
    
    }
    
    if (_highlightStyle == AXWireButtonHighlightStyleFilled) {
        [UIView animateWithDuration:0.25 animations:^{
            self.titleLabel.textColor = (!isTint ? [UIColor whiteColor] : textColor);
            self.backgroundColor = (!isTint ? textColor : [UIColor clearColor]);
        }];
    }
}

@end

//
//  AIRButton.m
//  hivery
//
//  Created by Artur on 22/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRButton.h"

@implementation AIRButton

- (instancetype)init {
    self = [super init];
    if (self) {
        [self p_prepare];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self p_prepare];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self p_prepare];
    }
    return self;
}

- (instancetype)initWithText:(NSString *)text {
    self = [super init];
    if (self) {
        [self p_prepare];
    }
    return self;
}

#pragma mark - Private

- (void)p_prepare {
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor lightGrayColor];
    self.tintColor = [UIColor whiteColor];
    self.layer.cornerRadius = 4;
}

@end

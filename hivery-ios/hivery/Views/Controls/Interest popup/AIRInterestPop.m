//
//  AIRInterestPop.m
//  hivery
//
//  Created by Artur on 04/10/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRInterestPop.h"

@interface AIRInterestPop ()

@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIView *popView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *interestedButton;

@property (copy, nonatomic) AIRClosePopBlock popupBlock;

@end

@implementation AIRInterestPop

#pragma mark - Initialization

//Override if View call init
- (instancetype)initWithDirection:(NSString *)direction closeCompletion:(AIRClosePopBlock)completion {
    self = [super init];
    if (self) {
        [self setupNib];
        self.popupBlock = completion;
        
        if ([direction isEqualToString:@"Right"]) {
            self.titleLabel.text = @"Interested?";
            self.messageLabel.text = @"Swiping right indicates that you are interested to view the home";
            [self.interestedButton setTitle:@"I'm interested" forState:UIControlStateNormal];
        } else {
            self.titleLabel.text  = @"Not interested?";
            self.messageLabel.text  = @"Swiping left indicates that you are not interested to view the home";
            [self.interestedButton setTitle:@"Not interested" forState:UIControlStateNormal];
        }
    }
    return self;
}

- (void)setupNib {
    
    UIView *view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(AIRInterestPop.class) owner:self options:nil]  firstObject];
    
    [self addSubview:view];
    
    self.popView.layer.cornerRadius = 8;
    self.popView.alpha = 0.95;
    
    view.frame = ScreenBounds;
}

#pragma mark - Events

- (IBAction)cancelButtonPressed:(id)sender {
    DLog(@"Cancel");
    if (self.popupBlock) {
        self.popupBlock(YES);
    }
}

- (IBAction)interestButtonPressed:(id)sender {
    DLog(@"Not interested");
    if (self.popupBlock) {
        self.popupBlock(NO);
    }
}

#pragma mark - Public


- (void)displayPopup {
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:self.view];
}

- (void)hidePopup {
    [self.view removeFromSuperview];
}


@end

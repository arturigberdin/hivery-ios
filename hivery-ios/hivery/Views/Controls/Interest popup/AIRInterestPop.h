//
//  AIRInterestPop.h
//  hivery
//
//  Created by Artur on 04/10/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AIRClosePopBlock)(BOOL isClosed);

@interface AIRInterestPop : UIView

- (instancetype)initWithDirection:(NSString *)direction closeCompletion:(AIRClosePopBlock)completion;

- (void)displayPopup;
- (void)hidePopup;

@end

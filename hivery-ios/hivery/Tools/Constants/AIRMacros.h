//
//  AIRMacros.h
//  hightech.fm
//
//  Created by Artur on 23/07/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#pragma mark - iPhone Device Number

#define IS_IPHONE_6             ([[UIScreen mainScreen] bounds].size.height == 667.f)
#define IS_IPHONE_6_PLUS        ([[UIScreen mainScreen] bounds].size.height == 736.f)
#define IS_IPHONE_5             ([[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_5_OR_HIGHER   ([[UIScreen mainScreen] bounds].size.height >= 568.0f)
#define IS_IPHONE_5_OR_LESS     ([[UIScreen mainScreen] bounds].size.height <= 568.0)
#define IS_IPHONE_4_OR_LESS     ([[UIScreen mainScreen] bounds].size.height < 568.0)


#pragma mark - iPad Constants

#define IS_IPAD                 (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)


#pragma mark - Screen scale

#define IS_RETINA               ([UIScreen mainScreen].scale >= 2)
#define IS_EXTRA_RETINA         ([UIScreen mainScreen].scale == 3)


#pragma mark - IOS Version

#define SYSTEM_VERSION          ([[[UIDevice currentDevice] systemVersion] floatValue])
#define IOS7                    (7.0 <= SYSTEM_VERSION && SYSTEM_VERSION < 8.0)
#define IOS8                    (8.0 <= SYSTEM_VERSION && SYSTEM_VERSION < 9.0)
#define IOS9                    (9.0 <= SYSTEM_VERSION && SYSTEM_VERSION < 10.0)
#define IOS7_OR_HIGHER          (7.0 <= SYSTEM_VERSION)
#define IOS8_OR_HIGHER          (8.0 <= SYSTEM_VERSION)
#define IOS9_OR_HIGHER          (9.0 <= SYSTEM_VERSION)


#pragma mark - Device Orientation

#define IS_PORTRAIT     UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])


#pragma mark - Debug Log

#ifdef DEBUG
#define DLog(...) NSLog(@"%s(%p) %@", __PRETTY_FUNCTION__, self, [NSString stringWithFormat:__VA_ARGS__])
#else
#define DLog(...)
#endif


#pragma mark - String Concat

#define StringConcat(...) [@[__VA_ARGS__] componentsJoinedByString:@""]


#pragma mark - Localization

#define NSLocalizedString(key, comment) [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:nil]


#pragma mark - Screen sizes

#define ScreenBounds      [UIScreen mainScreen].bounds
#define ScreenHeight      [UIScreen mainScreen].bounds.size.height
#define ScreenWidth       [UIScreen mainScreen].bounds.size.width
#define ScreenMaxHeight   MAX(ScreenWidth, ScreenHeight)
#define ScreenMaxWidth    MIN(ScreenWidth, ScreenHeight)

#pragma mark - Bar sizes

#define StatusBarHeight      20
#define NavigationBarHeight  44


#pragma mark - UIColor from HEX code

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
                 blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
                alpha:1.0]

// label.textColor = UIColorFromRGB(0xBC1128);

#pragma mark - Log method's execution time

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

#pragma mark - Alert

#define SHOW_ALERT(alertString) \
UIAlertController *alert = \
[UIAlertController alertControllerWithTitle:alertString message:nil preferredStyle:UIAlertControllerStyleAlert]; \
[alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) { }]]; \
[alert show];

#pragma mark - Log method

#define LOG_CMD NSLog(@"%@", NSStringFromSelector(_cmd));



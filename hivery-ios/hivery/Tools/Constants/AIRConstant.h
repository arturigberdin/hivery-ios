//
//  AIRConstant.h
//  hivery
//
//  Created by Artur on 22/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#ifndef AIRConstant_h
#define AIRConstant_h

static const NSUInteger AIRPasscodeLength = 4;

//Notifications

static NSString *const kChangeAccessToken = @"kChangeAccessToken";
static NSString *const kVerificationCode  = @"kVerificationCode";
static NSString *const kSearchParams      = @"kSearchParams";

//User Defaults
static NSString *const kPhoneNumber = @"kPhoneNumber";

static NSString *const kDeviceToken = @"kDeviceToken";

static NSString *const kUserInfoCached = @"kUserInfoCached";

static NSString *const kFirstInterest = @"kFirstInterest";
static NSString *const kFirstNotInterest = @"kFirstNotInterest";

static NSString *const kFirstLaunch = @"kFirstLaunch";

static NSString *const kUserAuthorized = @"kUserAuthorized";

#endif /* AIRConstant_h */

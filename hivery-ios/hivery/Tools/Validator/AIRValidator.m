//
//  AIRValidator.m
//  hivery
//
//  Created by Artur on 22/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRValidator.h"

@implementation AIRValidator

+ (BOOL)validatePhone:(NSString *)phone {
    
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    BOOL valid = [phoneTest evaluateWithObject:phone];
    
    return valid;
}

+ (BOOL)validateEmail:(NSString *)email {
    
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)validateName:(NSString *)username {
    
    //Remove all whitespaces
    NSString *name = [username stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //Symbols without special symbols, numbers and limit to 35
    BOOL lengthBool = (name.length < 35) ? YES : NO;
    
    NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'0123456789";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet characterSetWithCharactersInString:specialCharacterString];

    BOOL symbolBool;
    if ([name.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        symbolBool = NO;
    } else {
        symbolBool = YES;
    }
    
    if (lengthBool && symbolBool) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL)validateVerificationCode:(NSString *)code {
    BOOL isNumber = [self validateNumber:code];
    return (code.length == AIRPasscodeLength && isNumber);
}

+ (BOOL)validateNumber:(NSString *)number {
    NSScanner *scanner = [NSScanner scannerWithString:number];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    return isNumeric;
}

+ (BOOL)validatePassword:(NSString *)password {
    return (password.length >= 5) && (password.length <= 15);
}

@end

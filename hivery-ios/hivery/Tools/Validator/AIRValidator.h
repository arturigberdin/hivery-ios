//
//  AIRValidator.h
//  hivery
//
//  Created by Artur on 22/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIRValidator : NSObject

+ (BOOL)validatePhone:(NSString *)phone;

+ (BOOL)validateEmail:(NSString *)email;
+ (BOOL)validateName:(NSString *)name;

+ (BOOL)validatePassword:(NSString *)password;
+ (BOOL)validateVerificationCode:(NSString *)code;
+ (BOOL)validateNumber:(NSString *)number;

@end

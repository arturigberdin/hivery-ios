//
//  AIRAlert.m
//  hivery
//
//  Created by Artur on 11/10/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "AIRAlert.h"

@implementation AIRAlert

+ (void)alertWithMessage:(NSString *)message {
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        DLog(@"Title message alert!");
    }]];
    
    [alert show];
}

+ (void)alertWithTitle:(NSString *)title {
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        DLog(@"Title alert!");
    }]];
    
    [alert show];
}

+ (void)alertWithTitle:(NSString *)title message:(NSString *)message {
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        DLog(@"Title message alert!");
    }]];
    
    [alert show];
}

+ (void)alertCodeConfirmation {
    
    NSString *alertTitle = @"Verification Status";
    NSString *alertMessage = @"Your code is not verified \n Resend verification code if you're not remember it";
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    
    [alert show];
}

+ (void)alertNoInternetConnection {
    
    NSString *alertTitle = @"No Internet Connection";
    NSString *alertMessage = @"Please check your internet connection and try again";
    
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    
    [alert show];
}

+ (void)alertLogoutWithYES:(void (^)(BOOL logout))completion {
    
}

@end

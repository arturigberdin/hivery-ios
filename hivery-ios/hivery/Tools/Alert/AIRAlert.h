//
//  AIRAlert.h
//  hivery
//
//  Created by Artur on 11/10/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AIRAlert : NSObject

+ (void)alertWithMessage:(NSString *)message;
+ (void)alertWithTitle:(NSString *)title;

+ (void)alertWithTitle:(NSString *)title message:(NSString *)message;

+ (void)alertCodeConfirmation;
+ (void)alertNoInternetConnection;

+ (void)alertLogoutWithYES:(void (^)(BOOL logout))completion;

@end

//
//  UIView+LoadNib.h
//  FindTheGang
//
//  Created by Neo Apostol on 19/10/14.
//  Copyright (c) 2014 FindTheGangCorp. All rights reserved.
//

@interface UIView (LoadNib)

+(instancetype)loadFromNib;
+(instancetype)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass;

@end

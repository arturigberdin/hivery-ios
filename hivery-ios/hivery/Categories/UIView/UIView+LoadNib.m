//
//  UIView+LoadNib.m
//  FindTheGang
//
//  Created by Neo Apostol on 19/10/14.
//  Copyright (c) 2014 FindTheGangCorp. All rights reserved.
//

#import "UIView+LoadNib.h"

@implementation UIView (LoadNib)

+ (instancetype)loadFromNib
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:nil options:nil] firstObject];
}

+ (instancetype)loadNibNamed:(NSString *)nibName ofClass:(Class)objClass {
    
    if (nibName && objClass && [[NSBundle mainBundle] pathForResource:nibName ofType:@"nib"]) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:nibName
                                                         owner:nil
                                                       options:nil];
        for (id currentObject in objects ){
            if ([currentObject isKindOfClass:objClass])
                return currentObject;
        }
    }
    
    return nil;
}

@end

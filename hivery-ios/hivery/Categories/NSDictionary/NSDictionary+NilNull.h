//
//  NSDictionary+NilNull.h
//  hivery
//
//  Created by Artur on 15/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NilNull)

- (id)optionalObjectForKey:(id)key;
- (id)optionalObjectForKey:(id)key defaultValue:(id)defaultValue;

@end

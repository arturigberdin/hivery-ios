//
//  NSDictionary+NilNull.h
//  hivery
//
//  Created by Artur on 15/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "NSDictionary+NilNull.h"

@implementation NSDictionary (NilNull)

- (id)optionalObjectForKey:(id)key {
    return [self optionalObjectForKey:key defaultValue:nil];
}

- (id)optionalObjectForKey:(id)key defaultValue:(id)defaultValue {
    id obj = [self objectForKey:key];
    return (obj == [NSNull null] || !obj) ? defaultValue : obj;
}

@end

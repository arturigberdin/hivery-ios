//
//  UIViewController+Navigation.m
//  hivery
//
//  Created by Artur on 31/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "UIViewController+Navigation.h"

//Controllers
#import "AIRSetUpProfileVC.h"
#import "AIREstatesViewController.h"
#import "AIRLoginViewController.h"
#import "AIRProfileViewController.h"
#import "AIRScheduleViewController.h"

//Base
#import "AIRBaseTabBarController.h"
#import "AIRProfileNavigationController.h"

@implementation UIViewController (Navigation)

- (void)showQuestionsController {
    AIRSetUpProfileVC *questionsVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRSetUpProfileVC.class)];
    [self presentViewController:questionsVC animated:YES completion:nil];
}

- (void)showEstatesController {
    AIREstatesViewController *estatesVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIREstatesViewController.class)];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:estatesVC];
    [self presentViewController:navVC animated:YES completion:nil];
}

- (void)showLoginController {
    AIRLoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRLoginViewController.class)];
    //UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:loginVC animated:YES completion:nil];
}

- (void)showEstatesControllerFromTabBar {
    
    AIRProfileViewController *vc1 = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRProfileViewController.class)];
    AIREstatesViewController *vc2 = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIREstatesViewController.class)];
    AIRScheduleViewController *vc3 = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRScheduleViewController.class)];
    
    AIRProfileNavigationController *navVC1 = [[AIRProfileNavigationController alloc] initWithRootViewController:vc1];
    
    AIRProfileNavigationController *navVC3 = [[AIRProfileNavigationController alloc] initWithRootViewController:vc3];
    
    AIRBaseTabBarController *tabVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(AIRBaseTabBarController.class)];
    tabVC.viewControllers = [NSArray arrayWithObjects:navVC1, vc2, navVC3, nil];
    
    [self presentViewController:tabVC animated:YES completion:nil];
    
    //Show second tab in tabbar
    [tabVC setSelectedIndex:1];
}

@end

//
//  UIViewController+Navigation.h
//  hivery
//
//  Created by Artur on 31/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Navigation)

//Setup Your Profile (Register)
- (void)showQuestionsController;

//Cards (Feed)
- (void)showEstatesController;

//Login
- (void)showLoginController;

//TabBar
- (void)showEstatesControllerFromTabBar;

@end

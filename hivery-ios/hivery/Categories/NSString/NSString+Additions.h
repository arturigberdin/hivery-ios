//
//  NSString+Additions.h
//  hivery
//
//  Created by Artur on 05/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString *)removeLastString;

@end

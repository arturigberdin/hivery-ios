//
//  NSString+Trimming.m
//  hivery
//
//  Created by Artur on 05/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "NSString+Trimming.h"

@implementation NSString (Trimming)

- (NSString *)trimmingWhitespaces {
    
    NSString *rawString = self;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0) {
        // Text was empty or only whitespace.
    }
    return trimmed;
}

@end

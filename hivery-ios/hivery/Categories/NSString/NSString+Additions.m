//
//  NSString+Additions.m
//  hivery
//
//  Created by Artur on 05/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString *)removeLastString {
    
    NSString *string = self;
    if ([string length] > 0) {
        string = [self substringToIndex:[string length] - 1];
    }
    return string;
}

@end

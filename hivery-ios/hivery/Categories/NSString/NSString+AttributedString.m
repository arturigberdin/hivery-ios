//
//  NSString+AttributedString.m
//  hivery
//
//  Created by Artur on 02/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import "NSString+AttributedString.h"

@implementation NSString (AttributedString)

- (NSAttributedString *)attributedString {
    return [[NSAttributedString alloc] initWithString:self];
}

- (NSMutableAttributedString *)mutableAttributedString {
    return [[NSMutableAttributedString alloc] initWithString:self];
}

@end

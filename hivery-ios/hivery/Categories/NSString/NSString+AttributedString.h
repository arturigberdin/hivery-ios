//
//  NSString+AttributedString.h
//  hivery
//
//  Created by Artur on 02/09/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AttributedString)

- (NSAttributedString *)attributedString;
- (NSMutableAttributedString *)mutableAttributedString;

@end

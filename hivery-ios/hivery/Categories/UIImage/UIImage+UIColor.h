//
//  UIImage+UIColor.h
//  hivery
//
//  Created by Artur on 15/08/16.
//  Copyright © 2016 Arthur Igberdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIColor)

+ (UIImage *)imageWithUIColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageWithUIColor:(UIColor *)color;

@end
